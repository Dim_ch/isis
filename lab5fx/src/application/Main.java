package application;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ru.bstu.it32.anistratov.lab5.Country;
import ru.bstu.it32.anistratov.lab5.DbMySql;
import ru.bstu.it32.anistratov.lab5.DbXml;
import ru.bstu.it32.anistratov.lab5.PropReader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;


public class Main extends Application {
	private Country lastAddCountry = null;
	@Override
    public void init() throws Exception {
        super.init();
        
        PropReader.read("./resources/config.properties");
        
        DbMySql.setUrl(PropReader.getDbUrl());
        DbMySql.setPassword(PropReader.getDbPassword());
        DbMySql.setUsername(PropReader.getDbUsername());
        DbMySql.getInstance();
        
        DbXml.setFilePath(PropReader.getFileXml());
        DbXml.getInstance();
    }
    
	@Override
	public void start(Stage primaryStage) {
		try {
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("Sample.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			primaryStage.setTitle("Лабораторная работа №5");
			primaryStage.setScene(scene);
			primaryStage.show();
			
			setMainAppToController();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public boolean showCountryCreateDialog() {
		try {
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(Main.class.getResource("CountryCreateDialog.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	        
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Добавление страны");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initStyle(StageStyle.UTILITY);
	        
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	        
	        CountryCreateDialogController controller = loader.getController();
	        controller.setDialogStage(dialogStage);

	        // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
	        dialogStage.showAndWait();
	        
	        lastAddCountry = controller.getCountry();
	        
	        return controller.isOkClicked();
	        
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public void setMainAppToController() {
        // Загружаем сведения об адресатах.
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Sample.fxml"));
        
        try {
			loader.load();
			// Даём контроллеру доступ к главному приложению.
			SampleController controller = loader.getController();
			controller.setMainApp(this);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Country getAddedCountry() {
		Country country = lastAddCountry;
		lastAddCountry = null;
		return country;
	}
}
