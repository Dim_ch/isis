package application;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CountryItem {
	private final IntegerProperty id;
	private final StringProperty continent;
	private final StringProperty country;
	private final StringProperty capital;
	private final StringProperty minerals;
	private final StringProperty typeGovernment;
	private final StringProperty area;
	private final StringProperty population;
	
	public CountryItem() {
		this(null, null, null, null, null, null, null, null);
	}
	
	public CountryItem(Integer id, String continent, String country,
			String capital, String minerals, String typeGovernment,
			Integer area, Integer population) {
		this.id = new SimpleIntegerProperty(id);
		this.area = new SimpleStringProperty(area.toString());
		this.population = new SimpleStringProperty(population.toString());
		this.continent = new SimpleStringProperty(continent);
		this.country = new SimpleStringProperty(country);
		this.capital = new SimpleStringProperty(capital);
		this.minerals = new SimpleStringProperty(minerals);
		this.typeGovernment = new SimpleStringProperty(typeGovernment);
	}

	public int getId() {
		return id.get();
	}
	
	public void setId(int id) {
		this.id.set(id);
	}
	
	public IntegerProperty idProperty() {
		return id;
	}

	public String getContinent() {
		return continent.get();
	}
	
	public void setContinent(String continent) {
		this.continent.set(continent);
	}
	
	public StringProperty continentProperty() {
		return continent;
	}

	public String getCountry() {
		return country.get();
	}
	
	public void setCountry(String country) {
		this.country.set(country);
	}
	
	public StringProperty countryProperty() {
		return country;
	}

	public String getCapital() {
		return capital.get();
	}
	
	public void setCapital(String capital) {
		this.capital.set(capital);
	}
	
	public StringProperty capitalProperty() {
		return capital;
	}

	public String getMinerals() {
		return minerals.get();
	}
	
	public void setMinerals(String minerals) {
		this.minerals.set(minerals);
	}
	
	public StringProperty mineralsProperty() {
		return minerals;
	}

	public String getTypeGovernment() {
		return typeGovernment.get();
	}
	
	public void setTypeGovernment(String typeGovernment) {
		this.typeGovernment.set(typeGovernment);
	}
	
	public StringProperty typeGovernmentProperty() {
		return typeGovernment;
	}

	public String getArea() {
		return area.get();
	}
	
	public int getAreaN() {
		int number = 0;
		try {
			number = Integer.parseInt(getArea());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return number;
	}
	
	public void setArea(String area) {
		this.area.set(area);
	}
	
	public StringProperty areaProperty() {
		return area;
	}

	public String getPopulation() {
		return population.get();
	}
	
	public int getPopulationN() {
		int number = 0;
		try {
			number = Integer.parseInt(getPopulation());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return number;
	}
	
	public void setPopulation(String population) {
		this.population.set(population);
	}
	
	public StringProperty populationProperty() {
		return population;
	}
	
	/**
	 * Сравнивает по полю Id
	 */
    @Override
    public boolean equals(Object object)
    {
        boolean result = false;

        if (object != null && object instanceof CountryItem)
        {
        	result = this.getId() == ((CountryItem) object).getId();
        }

        return result;
    }
}
