package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ru.bstu.it32.anistratov.lab5.Country;

public class CountryCreateDialogController {
    private Stage dialogStage;
    private Country country = null;
    private boolean okClicked = false;
	
    @FXML
    private TextField textFieldContinent;

    @FXML
    private TextField textFieldCountry;

    @FXML
    private TextField textFieldCapital;

    @FXML
    private TextField textFieldTypeGovernment;

    @FXML
    private TextArea textAreaMinerals;

    @FXML
    private TextField textFieldPopulation;

    @FXML
    private TextField textFieldArea;

    @FXML
    void handleCancel(ActionEvent event) {
    	dialogStage.close();
    }

    @FXML
    void handleOk(ActionEvent event) {
        if (isInputValid()) {
        	country = new Country();
        	country.setArea(Integer.parseInt(textFieldArea.getText()));
        	country.setPopulation(Integer.parseInt(textFieldArea.getText()));
        	country.setContinent(textFieldContinent.getText());
        	country.setName(textFieldCountry.getText());
        	country.setCapital(textFieldCapital.getText());
        	country.setTypeGovernment(textFieldTypeGovernment.getText());
        	country.setMinerals(textAreaMinerals.getText());

            okClicked = true;
            dialogStage.close();
        }
    }
    
    private boolean isInputValid() {
        String errorMessage = "";

        if (textFieldContinent.getText() == null || textFieldContinent.getText().trim().isEmpty()) {
        	errorMessage += "Некорректные данные в поле 'Континент'\n";
        }
        
        if (textFieldCountry.getText() == null || textFieldCountry.getText().trim().isEmpty()) {
        	errorMessage += "Некорректные данные в поле 'Страна'\n";
        }
        
        if (textFieldCapital.getText() == null || textFieldCapital.getText().trim().isEmpty()) {
        	errorMessage += "Некорректные данные в поле 'Столица'\n";
        }
        
        if (textFieldTypeGovernment.getText() == null || textFieldTypeGovernment.getText().trim().isEmpty()) {
        	errorMessage += "Некорректные данные в поле 'Тип власти'\n";
        }
        
        if (textAreaMinerals.getText() == null || textAreaMinerals.getText().trim().isEmpty()) {
        	errorMessage += "Некорректные данные в поле 'Полезные ископаемые'\n";
        }

        if (textFieldPopulation.getText() == null || textFieldPopulation.getText().trim().isEmpty()) {
        	errorMessage += "Некорректные данные в поле 'Население'. Введите значение\n";
        } else {
        	try {
        		Integer.parseInt(textFieldPopulation.getText());
        	} catch (NumberFormatException  e) {
        		errorMessage += "Некорректные данные в поле 'Население' (должно быть целое число)\n";
			}
        }
        
        if (textFieldArea.getText() == null || textFieldArea.getText().trim().isEmpty()) {
        	errorMessage += "Некорректные данные в поле 'Площадь'. Введите значение\n";
        } else {
        	try {
        		Integer.parseInt(textFieldArea.getText());
        	} catch (NumberFormatException  e) {
        		errorMessage += "Некорректные данные в поле 'Площадь' (должно быть целое число)\n";
			}
        }
        
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Показываем сообщение об ошибке.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Некорректные данные");
            alert.setHeaderText("Пожалуйста введите корректные данные");
            alert.setContentText(errorMessage);
            
            alert.showAndWait();
            
            return false;
        }
    }
    
    public boolean isOkClicked() {
        return okClicked;
    }
    
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    public Country getCountry() {
    	return country;
    }
    
    @FXML
    private void initialize() {
    }
}
