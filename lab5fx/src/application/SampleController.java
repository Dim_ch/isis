package application;

import ru.bstu.it32.anistratov.lab5.Country;
import ru.bstu.it32.anistratov.lab5.DataBase;
import ru.bstu.it32.anistratov.lab5.DbMySql;
import ru.bstu.it32.anistratov.lab5.DbXml;
import ru.bstu.it32.anistratov.lab5.Filter;
import ru.bstu.it32.anistratov.lab5.PropReader;

import java.util.ArrayList;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.TextFieldTableCell;

public class SampleController {
	private static Main mainApp = null;
	
    private ObservableList<CountryItem> countriesData = FXCollections.observableArrayList();
    private ObservableList<DataBase> dataBases = FXCollections.observableArrayList();
    private ObservableList<OperatorFilter> operators = FXCollections.observableArrayList();
    private ArrayList<CountryItem> itemsEdited = new ArrayList<CountryItem>();
    private DataBase curentDb = null;
    private int filterMaxMinIndex = 1; 

    @FXML
    private TableView<CountryItem> tableCountries;

    @FXML
    private TableColumn<CountryItem, Integer> idColumn;

    @FXML
    private TableColumn<CountryItem, String> continentColumn;

    @FXML
    private TableColumn<CountryItem, String> countryColumn;

    @FXML
    private TableColumn<CountryItem, String> capitalColumn;

    @FXML
    private TableColumn<CountryItem, String> areaColumn;

    @FXML
    private TableColumn<CountryItem, String> populationColumn;

    @FXML
    private TableColumn<CountryItem, String> mineralsColumn;

    @FXML
    private TableColumn<CountryItem, String> typeGovernmentColumn;
    
    @FXML
    private ComboBox<DataBase> comboBoxDb;
    
    @FXML
    private Label labelCurendDb;
    
    @FXML
    private Label labelConvertInfo;
    
    @FXML
    private Button btnRefresh;
    
    @FXML
    private Button btnUpdate;
    
    @FXML
    private TextField textFieldContinent;

    @FXML
    private TextField textFieldCountry;

    @FXML
    private TextField textFieldCapital;

    @FXML
    private TextField textFieldTypeGovernment;

    @FXML
    private TextArea textAreaMinerals;

    @FXML
    private ComboBox<OperatorFilter> comboboxArea;

    @FXML
    private TextField textFieldArea;

    @FXML
    private ComboBox<OperatorFilter> comboboxPopulation;

    @FXML
    private TextField textFieldPopulation;

    @FXML
    private Button btnFilterFound;

    @FXML
    private Button btnFilterClear;
    
    @FXML
    private RadioButton rbuttonMaxArea;

    @FXML
    private RadioButton rbuttonMinArea;

    @FXML
    private RadioButton rbuttonMaxPopulation;

    @FXML
    private RadioButton rbuttonMinPopulation;

    @FXML
    private Button btnFindMaxMin;
    
    @FXML
    private Button btnDelete;
    
    @FXML
    private Button btnAdd;

    @FXML
    private Button btnConvert;
    
    @FXML
    void btnAddClick(ActionEvent event) {
    	if (mainApp == null) return;
    	
        boolean okClicked = mainApp.showCountryCreateDialog();
        
        if (okClicked) {
        	int id = curentDb.getLastId() + 1;
        	
        	Country country = mainApp.getAddedCountry();
            
        	if (country != null) {
        		country.setId(id);
        		
        		ArrayList<Country> countries = new ArrayList<>();
        		countries.add(country);
        		
        		if (curentDb.insert(countries)) {
        			countriesData.add(countryToItem(country));
            		Alert alert = new Alert(AlertType.INFORMATION);
            		alert.setTitle("Добавление");
            		alert.setHeaderText(null);
            		alert.setContentText("Запись была добавлена");
            		alert.showAndWait();
            		
            		return;
        		} else {
            		Alert alert = new Alert(AlertType.WARNING);
            		alert.setTitle("Добавление");
            		alert.setHeaderText(null);
            		alert.setContentText("Не удалось добавить запись");
            		alert.showAndWait();
        		}
        	}        
        }
        
        
    }

    @FXML
    void btnConvertClick(ActionEvent event) {
    	Alert alert = new Alert(null);
    	alert.setHeaderText(null);
    	
        if (DbMySql.isJDBCDriverReady()) {
        	DataBase dbFrom = comboBoxDb.getValue();
        	DataBase dbTo = null;
        	
        	if (dbFrom instanceof DbMySql) {
        		dbTo = DbXml.getInstance();
        	} else {
        		dbTo = DbMySql.getInstance();
        	}
        	
        	alert.setAlertType(AlertType.INFORMATION);
        	alert.setTitle("Конвертирование");
        	alert.setHeaderText("Конвертирование данных");
        	String message = String.format("Конвертирование данных из '%.40s...' -> '%.40s...'", dbFrom, dbTo);
        	
        	if (DataBase.convertData(dbFrom, dbTo)) {
        		message += " прошло успешно";
        	} else {
        		message += " не удалось";
        	}
        	
        	alert.setContentText(message);   	
        } else {
        	alert.setAlertType(AlertType.ERROR);
			alert.setTitle("Ошибка JDBC");
			alert.setContentText("JDBC драйвер не был загружен");
        }
        
        alert.showAndWait();
    }
    
    @FXML
    void btnDeleteClick(ActionEvent event) {
    	int selectedIndex = tableCountries.getSelectionModel().getSelectedIndex();
    	
    	if (selectedIndex == -1) {
    		Alert alert = new Alert(AlertType.WARNING);
    		alert.setTitle("Удаление");
    		alert.setHeaderText(null);
    		alert.setContentText("Выберите строку таблицы");
    		alert.showAndWait();
    		return;
    	}
    	
        CountryItem item = tableCountries.getItems().get(selectedIndex);
        DataBase db = comboBoxDb.getValue();
        
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Удаление");
        
        if (db.delete(item.getId())) {
        	tableCountries.getItems().remove(selectedIndex);
        	
    		alert.setContentText("Запись была удалена");
        } else {
        	alert.setContentText("Запись не была удалена");
        }
        
        alert.showAndWait();
    }
    
    @FXML
    void btnFindMaxMinClick(ActionEvent event) {
    	DataBase db = comboBoxDb.getValue();
    	Filter filter = db.createFilter();
    	Country country = null;
    	
    	switch (filterMaxMinIndex) {
		case 1: {
			country = filter.findCountryMaxArea(db);
			break;
		}
		case 2: {
			country = filter.findCountryMinArea(db);
			break;
		}
		case 3: {
			country = filter.findCountryMaxPopulation(db);
			break;
		}
		case 4: {
			country = filter.findCountryMinPopulation(db);
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + filterMaxMinIndex);
		}
    	
    	countriesData.clear();
    	countriesData.add(countryToItem(country));
    	itemsEdited.clear();
    }

    @FXML
    void btnFilterClearClick(ActionEvent event) {
    	filterClear();
    }

    @FXML
    void btnFilterFindClick(ActionEvent event) {
    	DataBase db = comboBoxDb.getValue();
    	Filter filter = db.createFilter();
    	
    	String str = textFieldArea.getText();
    	str = str.trim();
    	
    	if (!str.isEmpty()) {
    		try {
    			int number = Integer.parseInt(str);
    			
    			filter.addTermArea(number, comboboxArea.getValue().getOperationNumber());    			
    		} catch (Exception e) {
    			Alert alert = new Alert(AlertType.WARNING);
    			alert.setTitle("Ошибка в числовом поле");
    			alert.setHeaderText(String.format("Вы ввели '%s'", str));
    			alert.setContentText("Данную строку нельзя преобразовать в целое число");
    			alert.showAndWait();
    			
    			e.printStackTrace();
    			return;
			}
    		
    	}
    	
    	str = textFieldPopulation.getText();
    	str = str.trim();

    	if (!str.isEmpty()) {
    		try {
    			int number = Integer.parseInt(str);
    			
    			filter.addTermPopulation(number, comboboxPopulation.getValue().getOperationNumber());    			
    		} catch (Exception e) {
    			Alert alert = new Alert(AlertType.WARNING);
    			alert.setTitle("Ошибка в числовом поле");
    			alert.setHeaderText(String.format("Вы ввели '%s'", str));
    			alert.setContentText("Данную строку нельзя преобразовать в целое число");
    			alert.showAndWait();
    			
    			e.printStackTrace();
    			return;
			}
    		
    	}
    	
    	str = textFieldContinent.getText();
    	str = str.trim();
    	
    	if (!str.isEmpty()) {
    		filter.addTermContinent(str);
    	}
    	
    	str = textFieldCountry.getText();
    	str = str.trim();
    	
    	if (!str.isEmpty()) {
    		filter.addTermCountry(str);
    	}
    	
    	str = textFieldCapital.getText();
    	str = str.trim();
    	
    	if (!str.isEmpty()) {
    		filter.addTermCapital(str);
    	}
    	
    	str = textFieldTypeGovernment.getText();
    	str = str.trim();
    	
    	if (!str.isEmpty()) {
    		filter.addTermTypeGov(str);
    	}
    	
    	str = textAreaMinerals.getText();
    	str = str.trim();
    	
    	if (!str.isEmpty()) {
    		filter.addTermMinerals(str);
    	}
    	
    	ArrayList<Country> countries = filter.filter(db);
    	
    	if (countries.size() > 0) {
    		countriesData.clear();
    		countriesData.addAll(countriesToItems(countries));
    		itemsEdited.clear();
    	}
    	
    	Alert alert = new Alert(AlertType.INFORMATION);
    	alert.setHeaderText(null);
    	alert.setTitle("Поиск записей");
    	alert.setContentText("Количество найденных записей: " + countries.size());
    	alert.showAndWait();
    }
    
    @FXML
    void btnUpdateClick(ActionEvent event) {
    	String message = "";
    	int countEdited = 0;
//    	, countAdded = 0
    	
    	if (itemsEdited.size() > 0) {
    		ArrayList<Country> countriesEdited = new ArrayList<Country>();
    		
    		for (CountryItem countryItem : itemsEdited) {
    			countriesEdited.add(itemToCountry(countryItem));
    		}
    		
    		DataBase db = comboBoxDb.getValue();
    		
    		for (Country country : countriesEdited) {
    			if (db.update(country)) countEdited++;
			}
    		
    		if (countEdited > 0) itemsEdited.clear();
    		
    		message += String.format("Было обновлено записей: %d\n", countEdited);
    	} else {
    		message += String.format("Нет записей для обновления\n");
    	}
    	
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		alert.setTitle("Обновление БД");
		alert.setContentText(message);
		alert.showAndWait();
    }

    @FXML
    void btnRefreshClick(ActionEvent event) {
    	itemsEdited.clear();
    	
    	DataBase db = comboBoxDb.getValue();
    	
    	if (db instanceof DbMySql) {
    		if (!DbMySql.isJDBCDriverReady()) {
    			Alert alert = new Alert(AlertType.ERROR);
    			alert.setTitle("Ошибка JDBC");
    			alert.setContentText("JDBC драйвер не был загружен");
    			alert.showAndWait();
    			
    			return ;
    		}
    	}
    	
    	updateTable(db);
    }

    @FXML
    void comboBoxDbOnAction(ActionEvent event) {
    	DataBase db = comboBoxDb.getValue();
    	
    	if (db instanceof DbMySql) {
    		if (!DbMySql.isJDBCDriverReady()) {
    			comboBoxDb.setValue(curentDb);
    			
    			Alert alert = new Alert(AlertType.ERROR);
    			alert.setTitle("Ошибка JDBC");
    			alert.setContentText("JDBC драйвер не был загружен");
    			alert.showAndWait();
    			
    			return ;
    		}
    	}
    	
    	labelConvertInfo.setText(
    			String.format("'%.40s...' -> '%.40s...'", db, curentDb));
    	
    	curentDb = db;
    	labelCurendDb.setText(curentDb.toString());
    	btnRefreshClick(event);
    }
    
    @FXML
    void initialize() {
    	//====ComboBox====
    	DataBase dbXml = DbXml.getInstance();
    	DataBase dbSql = DbMySql.getInstance();
    	
    	dataBases.add(dbXml);
    	dataBases.add(dbSql);
    	
    	comboBoxDb.setItems(dataBases);
    	comboBoxDb.setValue(dbXml); // xml по умолчанию
    	curentDb = dbXml;
    	
    	
    	//====LabelConvertInfo====
    	labelConvertInfo.setText(String.format("'%.40s...' -> '%.40s...'", dbXml, dbSql));
    	
    	//====Label_labelCurendDb====
    	
    	labelCurendDb.setText(dbXml.toString());
    	
    	//====TableView====
    	ArrayList<Country> countries = dbXml.all();
    	countriesData.addAll(countriesToItems(countries));

		idColumn.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
		
		//====AREA(TEXTFIELD)====		
		areaColumn.setCellValueFactory(cellData -> cellData.getValue().areaProperty());
		areaColumn.setCellFactory(TextFieldTableCell.<CountryItem>forTableColumn());
		
		areaColumn.setOnEditCommit((CellEditEvent<CountryItem, String> event) -> {
			int row = event.getTablePosition().getRow();

			String strOld = event.getOldValue();
			String strNew = event.getNewValue();
			strNew = strNew.trim();
			
			CountryItem item = event.getTableView().getItems().get(row);
			
			try {
				int number = Integer.parseInt(strNew);
				
				if ((number >= PropReader.getMinNumber()) && (number <= PropReader.getMaxArea())) {
					item.setArea(strNew);
					
					if (!itemsEdited.contains(item))
						itemsEdited.add(item);
				}				
			} catch (Exception e) {
				item.setArea(strOld);
				e.printStackTrace();
			}
		});
		
		//====POPULATION(TEXTFIELD)====
		populationColumn.setCellValueFactory(cellData -> cellData.getValue().populationProperty());
		populationColumn.setCellFactory(TextFieldTableCell.<CountryItem>forTableColumn());
		
		populationColumn.setOnEditCommit((CellEditEvent<CountryItem, String> event) -> {
			int row = event.getTablePosition().getRow();

			String strOld = event.getOldValue();
			String strNew = event.getNewValue();
			strNew = strNew.trim();
			
			CountryItem item = event.getTableView().getItems().get(row);
			
			try {
				int number = Integer.parseInt(strNew);
				
				if ((number >= PropReader.getMinNumber()) && (number <= PropReader.getMaxArea())) {
					item.setPopulation(strNew);
					
					if (!itemsEdited.contains(item))
						itemsEdited.add(item);
				}
			} catch (Exception e) {
				item.setPopulation(strOld);
				e.printStackTrace();
			}
		});
		
		//====CONTINENT(TEXTFIELD)====
		continentColumn.setCellValueFactory(cellData -> cellData.getValue().continentProperty());
		continentColumn.setCellFactory(TextFieldTableCell.<CountryItem>forTableColumn());
		
		continentColumn.setOnEditCommit((CellEditEvent<CountryItem, String> event) -> {
			int row = event.getTablePosition().getRow();

			String strOld = event.getOldValue();
			String strNew = event.getNewValue();
			strNew = strNew.trim();
			
			CountryItem item = event.getTableView().getItems().get(row);
			
			if (!strNew.isEmpty()) {
				item.setContinent(strNew);
				
				if (!itemsEdited.contains(item))
					itemsEdited.add(item);
			} else {
				item.setContinent(strOld);
			}
		});
		
		//====COUNTRY(TEXTFIELD)====
		countryColumn.setCellValueFactory(cellData -> cellData.getValue().countryProperty());
		countryColumn.setCellFactory(TextFieldTableCell.<CountryItem>forTableColumn());
		
		countryColumn.setOnEditCommit((CellEditEvent<CountryItem, String> event) -> {
			int row = event.getTablePosition().getRow();

			String strOld = event.getOldValue();
			String strNew = event.getNewValue();
			strNew = strNew.trim();
			
			CountryItem item = event.getTableView().getItems().get(row);
			
			if (!strNew.isEmpty()) {
				item.setCountry(strNew);
				
				if (!itemsEdited.contains(item))
					itemsEdited.add(item);
			} else {
				item.setCountry(strOld);
			}
		});
		
		//====CAPITAL(TEXTFIELD)====
		capitalColumn.setCellValueFactory(cellData -> cellData.getValue().capitalProperty());
		capitalColumn.setCellFactory(TextFieldTableCell.<CountryItem>forTableColumn());
		
		capitalColumn.setOnEditCommit((CellEditEvent<CountryItem, String> event) -> {
			int row = event.getTablePosition().getRow();

			String strOld = event.getOldValue();
			String strNew = event.getNewValue();
			strNew = strNew.trim();
			
			CountryItem item = event.getTableView().getItems().get(row);
			
			if (!strNew.isEmpty()) {
				item.setCapital(strNew);
				
				if (!itemsEdited.contains(item))
					itemsEdited.add(item);
			} else {
				item.setCapital(strOld);
			}
		});
		
		//====MINERALS(TEXTFIELD)====
		mineralsColumn.setCellValueFactory(cellData -> cellData.getValue().mineralsProperty());
		mineralsColumn.setCellFactory(TextFieldTableCell.<CountryItem>forTableColumn());
		
		mineralsColumn.setOnEditCommit((CellEditEvent<CountryItem, String> event) -> {
			int row = event.getTablePosition().getRow();

			String strOld = event.getOldValue();
			String strNew = event.getNewValue();
			strNew = strNew.trim();
			
			CountryItem item = event.getTableView().getItems().get(row);
			
			if (!strNew.isEmpty()) {
				item.setMinerals(strNew);
				
				if (!itemsEdited.contains(item))
					itemsEdited.add(item);
			} else {
				item.setMinerals(strOld);
			}
		});
		
		//====TYPE_GOVERNMENT(TEXTFIELD)====
		typeGovernmentColumn.setCellValueFactory(cellData -> cellData.getValue().typeGovernmentProperty());
		typeGovernmentColumn.setCellFactory(TextFieldTableCell.<CountryItem>forTableColumn());
		
		typeGovernmentColumn.setOnEditCommit((CellEditEvent<CountryItem, String> event) -> {
			int row = event.getTablePosition().getRow();

			String strOld = event.getOldValue();
			String strNew = event.getNewValue();
			strNew = strNew.trim();
			
			CountryItem item = event.getTableView().getItems().get(row);
			
			if (!strNew.isEmpty()) {
				item.setTypeGovernment(strNew);
				
				if (!itemsEdited.contains(item))
					itemsEdited.add(item);
			} else {
				item.setTypeGovernment(strOld);
			}
		});
    	
    	tableCountries.setItems(countriesData);
    	
    	//====FILTERS====
    	operators.add(new OperatorFilter(0, ">"));
    	operators.add(new OperatorFilter(1, "<"));
    	operators.add(new OperatorFilter(2, "="));
    	
    	comboboxArea.setItems(operators);
    	comboboxPopulation.setItems(operators);
    	
    	filterClear();
    	
    	//====RadioButtonGroup====
    	ToggleGroup group = new ToggleGroup();
    	
        group.selectedToggleProperty().addListener((ChangeListener<? super Toggle>) new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
                if (group.getSelectedToggle() != null) {
                    RadioButton button = (RadioButton) group.getSelectedToggle();
                    String id = button.getId();
                    
                    if (id.equals("rbuttonMaxArea")) {
                    	filterMaxMinIndex = 1;
                    } else if (id.equals("rbuttonMinArea")) {
                    	filterMaxMinIndex = 2;
                    } else if (id.equals("rbuttonMaxPopulation")) {
                    	filterMaxMinIndex = 3;
                    } else if (id.equals("rbuttonMinPopulation")) {
                    	filterMaxMinIndex = 4;
                    }
                }
            }
        });
    	
    	rbuttonMaxArea.setToggleGroup(group);
    	rbuttonMaxArea.setId("rbuttonMaxArea");
    	
    	rbuttonMinArea.setToggleGroup(group);
    	rbuttonMinArea.setId("rbuttonMinArea");
    	
    	rbuttonMaxPopulation.setToggleGroup(group);
    	rbuttonMaxPopulation.setId("rbuttonMaxPopulation");
    	
    	rbuttonMinPopulation.setToggleGroup(group);
    	rbuttonMinPopulation.setId("rbuttonMinPopulation");
    	
    	rbuttonMaxArea.setSelected(true);
    }
    
    private void filterClear() {
    	textFieldContinent.setText("");
    	textFieldCountry.setText("");
    	textFieldCapital.setText("");
    	textFieldTypeGovernment.setText("");
    	textAreaMinerals.setText("");
    	textFieldArea.setText("0");
    	textFieldPopulation.setText("0");
    	
        comboboxArea.setValue(operators.get(0));
        comboboxPopulation.setValue(operators.get(0));
    }
    
    private void updateTable(DataBase db) {
    	ArrayList<Country> countries = db.all();
    	countriesData.clear();
    	countriesData.addAll(countriesToItems(countries));
    }
    
    private ObservableList<CountryItem> countriesToItems(ArrayList<Country> c) {
    	ObservableList<CountryItem> items = FXCollections.observableArrayList();
    	
    	for (Country country : c) {
    		items.add(countryToItem(country));
    	}
    	
    	return items;
    }
    
    private CountryItem countryToItem(Country c) {
    	return new CountryItem(c.getId(), c.getContinent(), c.getName(),
    			c.getCapital(), c.getMinerals(), c.getTypeGovernment(), c.getArea(),
    			c.getPopulation());
    }
    
    private Country itemToCountry(CountryItem item) {
    	Country country = new Country(item.getId(), item.getCountry(), item.getCapital(),
    			item.getContinent(), item.getTypeGovernment(), item.getMinerals(), item.getAreaN(), item.getPopulationN());
    	
    	return country;
    }
    
    public void setMainApp(Main mainApp) {
    	SampleController.mainApp = mainApp;
    }
    
    private class OperatorFilter {
    	private int operationNumber;
    	private String operationStr;
    	
    	public OperatorFilter(int operationNumber, String operationStr) {
    		this.operationNumber = operationNumber;
    		this.operationStr = operationStr;
    	}
    	
    	@Override
    		public String toString() {
    			return operationStr;
    		}

		public int getOperationNumber() {
			return operationNumber;
		}
    }
}
