package application;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import ru.bstu.it32.anistratov.lab3.*;

public class SampleController {
	private ArrayList<Human> humans = new ArrayList<Human>();
	
    @FXML
    private TextField boyName;

    @FXML
    private TextField boySurname;

    @FXML
    private TextField boyPatronymic;

    @FXML
    private TextField boySchool;

    @FXML
    private TextField boyClassroom;
    
    @FXML
    private TextField boyYear;

    @FXML
    private TextField boyMonth;

    @FXML
    private TextField boyDay;

    @FXML
    private Button btnAddBoy;

    @FXML
    private TextField studentName;

    @FXML
    private TextField studentSurname;

    @FXML
    private TextField studentPatronymic;

    @FXML
    private TextField studentHigherSchool;

    @FXML
    private TextField studentGroupName;

    @FXML
    private TextField studentGroupNumber;

    @FXML
    private TextField studentCreditCardNumber;
    
    @FXML
    private TextField studentYear;

    @FXML
    private TextField studentMonth;

    @FXML
    private TextField studentDay;

    @FXML
    private Button btnAddStudent;

    @FXML
    private TextField teacherName;

    @FXML
    private TextField teacherSurname;

    @FXML
    private TextField teacherPatronymic;

    @FXML
    private TextField teacherPlaceWork;

    @FXML
    private TextField teacherExperience;

    @FXML
    private TextField teacherPost;

    @FXML
    private TextField teacherCertificateNumber;
    
    @FXML
    private TextField teacherYear;

    @FXML
    private TextField teacherMonth;

    @FXML
    private TextField teacherDay;

    @FXML
    private Button btnAddTeacher;

    @FXML
    private TextField directorName;

    @FXML
    private TextField directorSurname;

    @FXML
    private TextField directorPatronymic;

    @FXML
    private TextField directorPlaceWork;

    @FXML
    private TextField directorExperience;

    @FXML
    private TextField directorSalary;

    @FXML
    private TextField directorCertificateNumber;
    
    @FXML
    private TextField directorYear;

    @FXML
    private TextField directorMonth;

    @FXML
    private TextField directorDay;

    @FXML
    private Button btnAddDirector;

    @FXML
    private TextArea textAreaHumans;
    
    @FXML
    private TextField textFieldHumanYoung;

    private void SetTextAreaHumans(Human human) {
    	String textHumans = textAreaHumans.getText() + human.toString();   
    	textAreaHumans.setText(textHumans);
    }
    
    private void GetHumanYoung() {
    	if (humans.size() == 0) return;
    	
		Human young = humans.get(0);
		
		for (int i = 0; i < humans.size() - 1; ++i) {
			if (young.getAge() > humans.get(i + 1).getAge())
				young = humans.get(i + 1);
		}
		
		textFieldHumanYoung.setText(young.toString());
    }
    
    @FXML
    void btnAddBoyClick(ActionEvent event) {
    	SchoolBoy human = new SchoolBoy();
    	human.setFirstName(boyName.getText());
    	human.setLastName(boySurname.getText());
    	human.setPatronymic(boyPatronymic.getText());
    	human.setSchool(boySchool.getText());
    	
		Calendar date = new GregorianCalendar();
		
    	try {
			String day = boyDay.getText();
			int number = Integer.parseInt(day);
			date.set(Calendar.DAY_OF_MONTH, number);
		} catch (Exception e) {
			boyDay.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String month = boyMonth.getText();
			int number = Integer.parseInt(month);
			date.set(Calendar.MONTH, number - 1);
		} catch (Exception e) {
			boyMonth.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String year = boyYear.getText();
			int number = Integer.parseInt(year);
			date.set(Calendar.YEAR, number);
		} catch (Exception e) {
			boyYear.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	human.setDateBirth(date);
    	
    	try {
			String classroom = boyClassroom.getText();
			int number = Integer.parseInt(classroom);
			human.setClassroom(number);
		} catch (Exception e) {
			boyClassroom.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	humans.add(human);
    	SetTextAreaHumans(human);
    	GetHumanYoung();
    }

    @FXML
    void btnAddDirectorClick(ActionEvent event) {
    	Director human = new Director();
    	human.setFirstName(directorName.getText());
    	human.setLastName(directorSurname.getText());
    	human.setPatronymic(directorPatronymic.getText());
    	human.setPlaceWork(directorPlaceWork.getText());
    	
		Calendar date = new GregorianCalendar();
		
    	try {
			String day = directorDay.getText();
			int number = Integer.parseInt(day);
			date.set(Calendar.DAY_OF_MONTH, number);
		} catch (Exception e) {
			directorDay.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String month = directorMonth.getText();
			int number = Integer.parseInt(month);
			date.set(Calendar.MONTH, number - 1);
		} catch (Exception e) {
			directorMonth.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String year = directorYear.getText();
			int number = Integer.parseInt(year);
			date.set(Calendar.YEAR, number);
		} catch (Exception e) {
			directorYear.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	human.setDateBirth(date);
    	
    	try {
			String exp = directorExperience.getText();
			int number = Integer.parseInt(exp);
			human.setExperience(number);
		} catch (Exception e) {
			directorExperience.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String salary = directorSalary.getText();
			int number = Integer.parseInt(salary);
			human.setExperience(number);
		} catch (Exception e) {
			directorSalary.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String certificate = directorCertificateNumber.getText();
			int number = Integer.parseInt(certificate );
			human.setExperience(number);
		} catch (Exception e) {
			directorCertificateNumber.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	humans.add(human);
    	SetTextAreaHumans(human);
    	GetHumanYoung();
    }

    @FXML
    void btnAddStudentClick(ActionEvent event) {
    	Student human = new Student();
    	human.setFirstName(studentName.getText());
    	human.setLastName(studentSurname.getText());
    	human.setPatronymic(studentPatronymic.getText());
    	human.setHigherSchool(studentHigherSchool.getText());
    	human.setGroupName(studentGroupName.getText());
    	
		Calendar date = new GregorianCalendar();
		
    	try {
			String day = studentDay.getText();
			int number = Integer.parseInt(day);
			date.set(Calendar.DAY_OF_MONTH, number);
		} catch (Exception e) {
			studentDay.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String month = studentMonth.getText();
			int number = Integer.parseInt(month);
			date.set(Calendar.MONTH, number - 1);
		} catch (Exception e) {
			studentMonth.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String year = studentYear.getText();
			int number = Integer.parseInt(year);
			date.set(Calendar.YEAR, number);
		} catch (Exception e) {
			studentYear.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	human.setDateBirth(date);
    	
    	try {
			String groupNumber = studentGroupNumber.getText();
			int number = Integer.parseInt(groupNumber);
			human.setGroupNumber(number);
		} catch (Exception e) {
			studentGroupNumber.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String creditNumber = studentCreditCardNumber.getText();
			int number = Integer.parseInt(creditNumber);
			human.setCreditCardNumber(number);
		} catch (Exception e) {
			studentCreditCardNumber.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	humans.add(human);
    	SetTextAreaHumans(human);
    	GetHumanYoung();
    }

    @FXML
    void btnAddTeacherClick(ActionEvent event) {
    	Teacher human = new Teacher();
    	human.setFirstName(teacherName.getText());
    	human.setLastName(teacherSurname.getText());
    	human.setPatronymic(teacherPatronymic.getText());
    	human.setPlaceWork(teacherPlaceWork.getText());
    	human.setPost(teacherPost.getText());
    	
		Calendar date = new GregorianCalendar();
		
    	try {
			String day = teacherDay.getText();
			int number = Integer.parseInt(day);
			date.set(Calendar.DAY_OF_MONTH, number);
		} catch (Exception e) {
			teacherDay.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String month = teacherMonth.getText();
			int number = Integer.parseInt(month);
			date.set(Calendar.MONTH, number - 1);
		} catch (Exception e) {
			teacherMonth.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String year = teacherYear.getText();
			int number = Integer.parseInt(year);
			date.set(Calendar.YEAR, number);
		} catch (Exception e) {
			teacherYear.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	human.setDateBirth(date);
    	
    	try {
			String exp = teacherExperience.getText();
			int number = Integer.parseInt(exp);
			human.setExperience(number);
		} catch (Exception e) {
			teacherExperience.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	try {
			String certificate = teacherCertificateNumber.getText();
			int number = Integer.parseInt(certificate );
			human.setExperience(number);
		} catch (Exception e) {
			teacherCertificateNumber.setText("Введите целое число");
			System.out.print(e.getMessage());
			return;
		}
    	
    	humans.add(human);
    	SetTextAreaHumans(human);
    	GetHumanYoung();
    }
}
