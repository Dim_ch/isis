package ru.bstu.it32.anistratov.lab5;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropReader {
	private static String fileXml;
	private static String warnCountry;
	private static String warnJDBC;
	private static String mainMenu;
	private static String filterMenu;
	private static String dbUrl;
	private static String dbUsername;
	private static String dbPassword;
	private static int minNumber;
	private static int maxArea;
	private static int maxPopulation;
	
	public static String getFileXml() {
		return fileXml;
	}
	public static String getWarnCountry() {
		return warnCountry;
	}
	public static String getWarnJDBC() {
		return warnJDBC;
	}
	public static String getMainMenu() {
		return mainMenu;
	}
	public static String getFilterMenu() {
		return filterMenu;
	}
	public static String getDbUrl() {
		return dbUrl;
	}
	public static String getDbUsername() {
		return dbUsername;
	}
	public static String getDbPassword() {
		return dbPassword;
	}
	public static int getMinNumber() {
		return minNumber;
	}
	public static int getMaxArea() {
		return maxArea;
	}
	public static int getMaxPopulation() {
		return maxPopulation;
	}
	
	public static void read(String fileProp) {
		Properties prop = new Properties();
        try {
            //обращение к файлу и получение данных
            FileInputStream fis = new FileInputStream(fileProp);
            prop.load(fis);
            
            fileXml = prop.getProperty("fileXml", "");
            
            warnCountry = new String(prop
            		.getProperty("warnCountryNotFound", "").getBytes("ISO8859-1"));
            
            warnJDBC = new String(prop
            		.getProperty("warnJDBCDriver", "").getBytes("ISO8859-1"));
            
            mainMenu = new String(prop.getProperty("infoMainMenu", "")
            		.getBytes("ISO8859-1"));
            
            filterMenu = new String(prop.getProperty("infoFilterMenu", "")
            		.getBytes("ISO8859-1"));
            
            dbUrl = new String(prop.getProperty("dbUrl", "")
            		.getBytes("ISO8859-1"));
            
            dbUsername = new String(prop.getProperty("dbUsername", "")
            		.getBytes("ISO8859-1"));
            
            dbPassword = new String(prop.getProperty("dbPassword", "")
            		.getBytes("ISO8859-1"));
            
            String value = prop.getProperty("minNumber");
            try {
            	minNumber = Integer.parseInt(value);
            } catch (Exception e) {
            	e.printStackTrace();
			}
            
            value = prop.getProperty("maxArea");
            try {
            	maxArea = Integer.parseInt(value);
            } catch (Exception e) {
            	e.printStackTrace();
			}
            
            value = prop.getProperty("maxPopulation");
            try {
            	maxPopulation = Integer.parseInt(value);
            } catch (Exception e) {
            	e.printStackTrace();
			}
        } catch (IOException e) {
            System.out.println("Ошибка в программе: файл .properties не найден");
            e.printStackTrace();
        } finally {
		}
	}
}
