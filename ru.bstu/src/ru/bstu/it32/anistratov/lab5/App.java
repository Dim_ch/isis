package ru.bstu.it32.anistratov.lab5;

import java.util.ArrayList;
import java.util.Scanner;

public class App {
	private static int MIN_NUMBER;
	private static int MAX_AREA;
	private static int MAX_POPULATION;
	private static String FILE_XML;
	private static String WARN_COUNTRY_NOT_FOUND;
	private static String WARN_JDBC_DRIVER;
	private static String MAIN_MENU;
	private static String FILTER_MENU;
	private static String DB_URL;
	private static String DB_USERNAME;
	private static String DB_PASSWORD;
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		getProperties();
		
		DbMySql.setUrl(DB_URL);
		DbMySql.setUsername(DB_USERNAME);
		DbMySql.setPassword(DB_PASSWORD);
		DbMySql dbMysql = DbMySql.getInstance();
		
		DbXml dbXml = DbXml.getInstance();
		DbXml.setFilePath(FILE_XML);
		
		DataBase currentDb = dbXml;
		
		boolean flag = true;
		while(flag) {
			showMenu(currentDb);
			int choice = getNumber(scan, 0, "Выберите действие: ");
			
			switch (choice) {
			case 0: // Выход
				flag = false;
				break;
			case 1: // Показать записи из БД
				showData(currentDb);				
				break;
			case 2: // Добавление записи
				insertCountry(scan, currentDb);
				break;
			case 3: // Изменение записи
				updateCountry(scan, currentDb);
				break;
			case 4: // Удаление записи
				deleteCountry(scan, currentDb);
				break;
			case 5: // Поиск по параметрам
				filterCountries(scan, currentDb);
				break;
			case 6: // Смена активной БД
				currentDb = changeDb(currentDb, dbXml, dbMysql);
				break;
			case 7: // Конвертирование данных
				convertData(currentDb, dbXml, dbMysql);
				break;
			default:
				System.out.println("Нет такого действия");
				break;
			}
		}

		System.out.println("Конец программы");
		scan.close();
	}
	
	private static void getProperties() {
		PropReader.read("./resources/config.properties");
            
        FILE_XML = PropReader.getFileXml();
        
        WARN_COUNTRY_NOT_FOUND = PropReader.getWarnCountry();
        
        WARN_JDBC_DRIVER = PropReader.getWarnJDBC();
        
        MAIN_MENU = PropReader.getMainMenu();
        
        FILTER_MENU = PropReader.getFilterMenu();
        
        DB_URL = PropReader.getDbUrl();
        
        DB_USERNAME = PropReader.getDbUsername();
        
        DB_PASSWORD = PropReader.getDbPassword();

    	MIN_NUMBER = PropReader.getMinNumber();

    	MAX_AREA = PropReader.getMaxArea();

    	MAX_POPULATION = PropReader.getMaxPopulation();

	}
	
	/**
	 * Считывает число с консоли, которое больше либо равно min
	 * @param scanner
	 * @param min минимальное число
	 * @param msg сообщение для приглашения ввода
	 * @return
	 */
	private static int getNumber(Scanner scanner, int min, String msg) {
		int number = min;
		String str = "";
		while(true) {
			System.out.print(msg);
			str = scanner.nextLine();
			try {
				number = Integer.parseInt(str);
				if (number >= min) break;
				else System.out.printf("Значение должно быть больше либо "
						+ "равно %d\n", min);
			} catch (Exception e) {
				System.out.printf("Не удалось преобразовать %s в число\n", str);
			}			
		}
		return number;
	}
	
	private static void showMenu(DataBase db) {
		System.out.println("");
		System.out.println("----" + db + "----");
		System.out.println(MAIN_MENU);
		System.out.println("----------------");
		System.out.println("");
	}
	
	private static void showMenuFilterNumber() {
		System.out.println("");
		System.out.println(FILTER_MENU);
		System.out.println("");
	}
	
	private static void showData(DataBase db) {
		ArrayList<Country> countries = db.all();
		
		if (countries.size() > 0) {
			for (Country country : countries) {
				System.out.println("");
				System.out.println(country);
			}
		}
		else 
			System.out.println(db + " пуста");
	}
	
	private static void insertCountry(Scanner scan, DataBase db) {
		ArrayList<Country> countries = new ArrayList<>();
		Country country = getCountry(scan, db);
		countries.add(country);
		
		if (db.insert(countries))
			System.out.println("Запись была добавлена в " + db);
		else
			System.out.println("Не удалось добавить запись в " + db);
	}
	
	private static void deleteCountry(Scanner scan, DataBase db) {
		int id = getNumber(scan, 0, "Введите id: ");
		
		if (db.delete(id))
			System.out.println("Запись была удалена из " + db);
		else
			System.out.println("Нет записи с id = " + id + " в " + db);
	}
	
	private static void updateCountry(Scanner scan, DataBase db) {
		int id = getNumber(scan, 0, "Введите id: ");
		Country country = db.getCountry(id);
		
		if (country != null) {
			editCountry(scan, country);
			
			if (db.update(country))
				System.out.println("Запись была обновлена в " + db);
			else
				System.out.println("Запись не была обновлена в " + db);
		}
		else
			System.out.println("Нет записи с id = " + id + "\n");
	}
	
	/**
	 * Считывает с консоли информацию о стране
	 * @param scan
	 * @return объект Country
	 */
	private static Country getCountry(Scanner scan, DataBase db) {
		Country country = new Country();
		
		System.out.print("Введите континент: ");
		country.setContinent(scan.nextLine());
		
		System.out.print("Введите название страны: ");
		country.setName(scan.nextLine());
		
		System.out.print("Введите название столицы: ");
		country.setCapital(scan.nextLine());
		
		int number;
		
		while (true) {
			number = getNumber(scan, 0, "Введите площадь: ");
			if ((number >= MIN_NUMBER) && (number <= MAX_AREA)) break;
			System.out.println("Значение должно быть в диапозоне от " 
					+ MIN_NUMBER + "до " + MAX_AREA);
		}
		
		country.setArea(number);
		
		while (true) {
			number = getNumber(scan, 0, "Введите население: ");
			if ((number >= MIN_NUMBER) && (number <= MAX_POPULATION)) break;
			System.out.println("Значение должно быть в диапозоне от " 
					+ MIN_NUMBER + "до " + MAX_POPULATION);
		}
		
		country.setPopulation(number);
		
		System.out.print("Введите тип власти: ");
		country.setTypeGovernment(scan.nextLine());
		
		System.out.print("Введите полезные ископаемые: ");
		country.setMinerals(scan.nextLine());
		country.setId(db.getLastId() + 1);
		
		return country;
	}
	
	/**
	 * Спрашивает новые значения полей с консоли и изменяет объект country
	 * @param scan
	 * @param country
	 */
	private static void editCountry(Scanner scan, Country country) {
		System.out.println("Страна:\n\n" + country + "\n");
		
		System.out.println("Изменить континент?");
		
		if (getYesOrNo(scan)) {
			System.out.print("Введите континент: ");
			country.setContinent(scan.nextLine());
		}
		
		System.out.println("Изменить страну?");
		
		if (getYesOrNo(scan)) {
			System.out.print("Введите название страны: ");
			country.setName(scan.nextLine());
		}
		
		System.out.println("Изменить столицу?");
		
		if (getYesOrNo(scan)) {
			System.out.print("Введите название столицы: ");
			country.setCapital(scan.nextLine());
		}
		
		System.out.println("Изменить площадь?");
		
		if (getYesOrNo(scan)) {
			int number;
			
			while (true) {
				number = getNumber(scan, 0, "Введите площадь: ");
				
				if ((number >= MIN_NUMBER) && (number <= MAX_AREA)) break;
				
				System.out.println("Значение должно быть в диапозоне от " 
						+ MIN_NUMBER + "до " + MAX_AREA);
			}
			
			country.setArea(number);
		}
		
		System.out.println("Изменить население?");
		
		if (getYesOrNo(scan)) {
			int number;
			
			while (true) {
				number = getNumber(scan, 0, "Введите население: ");
				
				if ((number >= MIN_NUMBER) && (number <= MAX_POPULATION)) break;
				
				System.out.println("Значение должно быть в диапозоне от " 
						+ MIN_NUMBER + "до " + MAX_POPULATION);
			}
			country.setPopulation(number);
		}
		
		System.out.println("Изменить тип власти?");
		
		if (getYesOrNo(scan)) {
			System.out.print("Введите тип власти: ");
			country.setTypeGovernment(scan.nextLine());
		}
		
		System.out.println("Изменить полезные ископаемые?");
		
		if (getYesOrNo(scan)) {
			System.out.print("Введите полезные ископаемые: ");
			country.setMinerals(scan.nextLine());
		}
	}
	
	private static boolean getYesOrNo(Scanner scan) {
		boolean yesNo = false;
		
		while(true) {
			System.out.println("0 - да\n1 - нет");
			int number = getNumber(scan, 0, "Введите значение: ");
			
			if (number == 0) {
				yesNo = true;
				break;
			} else if (number == 1) {
				yesNo = false;
				break;
			}
		}
		
		return yesNo;
	}

	private static Filter getFilterCountries(Scanner scan, DataBase db) {
		Filter filter = db.createFilter();
		
		System.out.println("Поиск по континенту?");
		
		if (getYesOrNo(scan)) {
			System.out.print("Введите континент: ");
			String str = scan.nextLine();
			filter.addTermContinent(str);
		}
		
		System.out.println("Поиск по стране?");
		
		if (getYesOrNo(scan)) {
			System.out.print("Введите название страны: ");
			String str = scan.nextLine();
			filter.addTermCountry(str);
		}
		
		System.out.println("Поиск по столице?");
		
		if (getYesOrNo(scan)) {
			System.out.print("Введите название столицы: ");
			String str = scan.nextLine();
			filter.addTermCapital(str);
		}
		
		System.out.println("Поиск по площади?");
		
		if (getYesOrNo(scan)) {
			showMenuFilterNumber();
			int operation = getNumber(scan, 0, "Выберите операцию: ");
			int number = getNumber(scan, 0, "Введите площадь: ");
			filter.addTermArea(number, operation);
		}
		
		System.out.println("Поиск по населению?");
		
		if (getYesOrNo(scan)) {
			showMenuFilterNumber();
			int operation = getNumber(scan, 0, "Выберите операцию: ");
			int number = getNumber(scan, 0, "Введите население: ");
			filter.addTermPopulation(number, operation);
		}
		
		System.out.println("Поиск по типу власти?");
		
		if (getYesOrNo(scan)) {
			System.out.print("Введите тип власти: ");
			String str = scan.nextLine();
			filter.addTermTypeGov(str);
		}
		
		System.out.println("Поиск по полезным ископаемым?");
		
		if (getYesOrNo(scan)) {
			System.out.print("Введите полезные ископаемые: ");
			String str = scan.nextLine();
			filter.addTermMinerals(str);
		}
		
		return filter;
	}

	private static void filterCountries(Scanner scan, DataBase db) {
		System.out.println("Поиск по нескольким параметрам?");
		
		if (getYesOrNo(scan)) {
			Filter filter = getFilterCountries(scan, db);
			
			ArrayList<Country> countries = filter.filter(db);
			
			System.out.println("Найденные страны:");
			
			if (countries.size() == 0) {
				System.out.println(WARN_COUNTRY_NOT_FOUND);
			} else {
				for (Country country : countries) {
					System.out.println(country);
				}				
			}
		} else {
			Country country = findCountryMinMaxNumber(scan, db);
			System.out.println(country);
		}
	}
	
	/**
	 * Функция ищет страну с максимальным или минимальным числовым значением.
	 * Поле выбирает пользователь
	 * @param scan
	 * @param db DataBase
	 * @return Country
	 */
	private static Country findCountryMinMaxNumber(Scanner scan, DataBase db) {
		System.out.println("1 - Поиск страны с максимальной площадью");
		System.out.println("2 - Поиск страны с минимальной площадью");
		System.out.println("3 - Поиск страны с максимальным населением");
		System.out.println("4 - Поиск страны с минимальным населением");

		Filter filter = db.createFilter();
		Country country = null;
		
		while (true) {
			int number = getNumber(scan, 1, "Введите число: ");
			
			if (number == 1) {
				country = filter.findCountryMaxArea(db);
				break;
			}
			
			if (number == 2) {
				country = filter.findCountryMinArea(db);
				break;
			}
			
			if (number == 3) {
				country = filter.findCountryMaxPopulation(db);
				break;
			}
			
			if (number == 4) {
				country = filter.findCountryMinPopulation(db);
				break;
			}
		}
		
		return country;
	}

	private static DataBase changeDb(DataBase curDb, DbXml dbXml, DbMySql Mysql) {
		DataBase newDb = null;
		
		if (curDb instanceof DbXml) {
			
			if (DbMySql.isJDBCDriverReady())
				newDb = Mysql;
			else {
				System.out.println(WARN_JDBC_DRIVER);
				newDb = dbXml;
			}
		} else
			newDb = dbXml;
		
		return newDb;
	}
	
	private static void convertData(DataBase curDb, DbXml dbXml, DbMySql Mysql) {
		boolean flag = false;
		
		if (DbMySql.isJDBCDriverReady()) {
			
			if (curDb instanceof DbXml)
				flag = DataBase.convertData(curDb, Mysql);
			else
				flag = DataBase.convertData(curDb, dbXml);
		} else {
			System.out.println(WARN_JDBC_DRIVER);
		}
		
		if (flag) {
			String message = String.format("Конвертация данных из %s в %s прошла успешно",
					curDb, (curDb instanceof DbXml) ? Mysql : dbXml);
			System.out.println(message);
		}
	}
}
