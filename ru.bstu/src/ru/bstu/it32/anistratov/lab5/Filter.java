package ru.bstu.it32.anistratov.lab5;

import java.util.ArrayList;

public interface Filter {
	public void addTermContinent(String str);
	public void addTermCountry(String str);
	public void addTermCapital(String str);
	public void addTermTypeGov(String str);
	public void addTermMinerals(String str);
	public void addTermArea(int number, int operation);
	public void addTermPopulation(int number, int operation);
	
	/**
	 * Выполняет поиск с заданными условиями
	 * @param countries
	 * @return ArrayList<Country> отфильтрованные элементы
	 */
	public ArrayList<Country> filter(DataBase db);
	
	public Country findCountryMaxArea(DataBase db);
	public Country findCountryMinArea(DataBase db);
	public Country findCountryMaxPopulation(DataBase db);
	public Country findCountryMinPopulation(DataBase db);
}
