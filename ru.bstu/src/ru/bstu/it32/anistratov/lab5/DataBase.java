package ru.bstu.it32.anistratov.lab5;

import java.util.ArrayList;

public interface DataBase {
	
	/**
	 * Считывает все записи из БД и возвращает список стран
	 * @return ArrayList Country
	 */
	public ArrayList<Country> all();
	
	/**
	 * Добавляет данные в БД. 
	 * @param Countries список стран
	 * @return результат работы метода (true/false)
	 */
	public boolean insert(ArrayList<Country> countries);
	
	/**
	 * Редактирует запись в файле
	 * 
	 * @param country редактируемый объект
	 * @return результат работы метода (true/false)
	 */
	public boolean update(Country country);
	
	/**
	 * Удаляет запись из xml по id.
	 * 
	 * @param id идентификатор элемента
	 * @return результат работы метода (true/false)
	 */
	public boolean delete(int id);
	
	/**
	 * Удаляет все записи из таблицы
	 * @return
	 */
	public boolean truncate();
	
	public Country getCountry(int id);
	
	public String toString();
	
	public static boolean convertData(DataBase dbFrom, DataBase dbTo) {
		boolean flag = false;
		
		ArrayList<Country> countries = dbFrom.all();

		if (dbTo.truncate()) {
			if (dbTo.insert(countries)) flag = true;
		}
		
		return flag;
	}
	
	public Filter createFilter();

	int getLastId();
}
