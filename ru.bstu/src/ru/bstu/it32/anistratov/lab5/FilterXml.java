package ru.bstu.it32.anistratov.lab5;

import java.util.ArrayList;
import java.util.function.Predicate;

public class FilterXml implements Filter {
	
	private ArrayList<Predicate<Country>> predicates;
	
	public FilterXml() {
		predicates = new ArrayList<Predicate<Country>>();
	}

	@Override
	public void addTermContinent(String str) {
		String string = str.toLowerCase();
		predicates.add(x -> x.getContinent().toLowerCase().contains(string));
		
	}

	@Override
	public void addTermCountry(String str) {
		String string = str.toLowerCase();
		predicates.add(x -> x.getName().toLowerCase().contains(string));			
	}

	@Override
	public void addTermCapital(String str) {
		String string = str.toLowerCase();
		predicates.add(x -> x.getCapital().toLowerCase().contains(string));
	}

	@Override
	public void addTermTypeGov(String str) {
		String string = str.toLowerCase();
		predicates.add(x -> x.getTypeGovernment().toLowerCase().contains(string));
	}

	@Override
	public void addTermMinerals(String str) {
		String string = str.toLowerCase();
		String[] minerals = string.split(",");
		
		for (int i = 0; i < minerals.length; i++) {
			String mineral = minerals[i];
			minerals[i] = mineral.trim();
		}
		
		Predicate<Country> pred = x -> {
			boolean flag = true;
			String mineralCountry = x.getMinerals().toLowerCase();
			for (String mineral : minerals) {
				if (!mineralCountry.contains(mineral))
					flag = false;
			}
			
			return flag;
		};
		
		predicates.add(pred);
	}

	@Override
	public void addTermArea(int number, int operation) {
		Predicate<Country> pred;
		
		switch (operation) {
		case 0: // больше
			pred = x -> x.getArea() > number;
			break;

		case 1: // меньше
			pred = x -> x.getArea() < number;
			break;
			
		default: // равно
			pred = x -> x.getArea() == number;
			break;
		}
		
		predicates.add(pred);
	}

	@Override
	public void addTermPopulation(int number, int operation) {
		Predicate<Country> pred;
		
		switch (operation) {
		case 0: // больше
			pred = x -> x.getPopulation() > number;
			break;

		case 1: // меньше
			pred = x -> x.getPopulation() < number;
			break;
			
		default: // равно
			pred = x -> x.getPopulation() == number;
			break;
		}
		
		predicates.add(pred);
	}
	
	@Override
	public ArrayList<Country> filter(DataBase db) {
		ArrayList<Country> filterCountry = new ArrayList<Country>();
		Predicate<Country> condition = getCondition();
		
		if (condition != null) {
			ArrayList<Country> countries = db.all();
			
			if (countries.size() > 0) {
				countries.forEach(item -> {
					if (condition.test(item))
						filterCountry.add(item);
				});
			}
		}
		
		return filterCountry;
	}
	
	/**
	 * Связывает все предикаты логическим И
	 * @return Predicate<Country> предикат с единым условием
	 */
	private Predicate<Country> getCondition() {
		Predicate<Country> condition = null;
		
		if (predicates.size() > 0) {
			condition = predicates.get(0);
			
			if (predicates.size() > 1) {
				for (int i = 1; i < predicates.size(); i++) {
					condition = condition.and(predicates.get(i));
				}				
			}
		}

		return condition;
	}
	
	private interface filterCountryNumber {
		boolean compare(Country c1, Country c2);
	}

	// Лямбда выражения для сравнения числовых свойств экземпляров.
	private static filterCountryNumber maxArea = (Country m1, Country m2)-> m1.getArea() > m2.getArea();
	private static filterCountryNumber minArea = (Country m1, Country m2)-> m1.getArea() < m2.getArea();
	private static filterCountryNumber maxPopulation = (Country m1, Country m2)-> m1.getPopulation() > m2.getPopulation();
	private static filterCountryNumber minPoulation = (Country m1, Country m2)-> m1.getPopulation() < m2.getPopulation();
	
	private static Country findCountry(filterCountryNumber filter, ArrayList<Country> countries) {
		Country country = null;
		
		if (countries.size() > 0) {
			country = countries.get(0);
			for (Country c : countries) {
				if (filter.compare(c, country))
					country = c;
			}
		}
		
		return country;
	}

	@Override
	public Country findCountryMaxArea(DataBase db) {
		return findCountry(maxArea, db.all());
	}

	@Override
	public Country findCountryMinArea(DataBase db) {
		return findCountry(minArea, db.all());
	}

	@Override
	public Country findCountryMaxPopulation(DataBase db) {
		return findCountry(maxPopulation, db.all());
	}

	@Override
	public Country findCountryMinPopulation(DataBase db) {
		return findCountry(minPoulation, db.all());
	}
	
	
	
}
