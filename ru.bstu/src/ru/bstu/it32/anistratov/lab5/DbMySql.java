package ru.bstu.it32.anistratov.lab5;

import java.sql.*;
import java.util.ArrayList;

public class DbMySql implements DataBase {
	private static volatile DbMySql instance;
	
	private DbMySql() {
		DbMySql.loadJDBCDriver();
	}
	
	public static DbMySql getInstance() {
		DbMySql result = instance;
		
		if (result != null) {
			return result;
		}
		
		synchronized (DbMySql.class) {
            if (instance == null) {
                instance = new DbMySql();
            }
            
            return instance;	
		}
	}
	
	private static String url;
	private static String username;
	private static String password;
	private static boolean JDBCDriverReady = false;
	

	public static String getUrl() {
		return url;
	}

	public static void setUrl(String url) {
		DbMySql.url = url;
	}

	public static String getUsername() {
		return username;
	}

	public static void setUsername(String username) {
		DbMySql.username = username;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		DbMySql.password = password;
	}

	/**
	 * @return the jDBCDriverReady
	 */
	public static boolean isJDBCDriverReady() {
		return JDBCDriverReady;
	}

	/**
	 * Загружает драйвер JDBC
	 * @return false - в случае неудачи, иначе true
	 */
	public static boolean loadJDBCDriver() {
		boolean flag = true;
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			JDBCDriverReady = true;
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		}
		
		return flag;
	}
	
	private Connection connection;
	/**
	 * Устанавливает соединение с БД. JDBC драйвер должен быть загружен.
	 * @return
	 */
	private boolean connect() {
		boolean flag = true;
		
		try {
			connection = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			flag = false;
			e.printStackTrace();
		}
		
		return flag;
	}
	
	private boolean closeConnection() {
		boolean flag = true;
		
		try {
			connection.close();			
		} catch (SQLException e) {
			e.printStackTrace();
			flag = false;
		}
		
		return flag;
	}
	
	private ArrayList<Country> readCountries(ResultSet rs) throws SQLException {
		ArrayList<Country> countries = new ArrayList<Country>();
		
		while (rs.next()) {
			Country country = new Country();
			
			country.setId(rs.getInt("Id"));
			country.setArea(rs.getInt("Area"));
			country.setPopulation(rs.getInt("Population"));
			country.setCapital(rs.getString("Capital"));
			country.setName(rs.getString("Country"));
			country.setContinent(rs.getString("Continent"));
			country.setMinerals(rs.getString("Minerals"));
			country.setTypeGovernment(rs.getString("TypeGovernment"));
			
			countries.add(country);
		}
		
		return countries;
	}
	
	public ArrayList<Country> query(String sqlCommand) {
		ArrayList<Country> countries = new ArrayList<Country>();
		
		if (this.connect()) {
			
			try {
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(sqlCommand);
				
				countries = readCountries(rs);
				
				rs.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			this.closeConnection();
		}
		
		return countries;
	}
	
	@Override
	public ArrayList<Country> all() {
		ArrayList<Country> countries = new ArrayList<Country>();
		
		if (this.connect()) {
			
			try {
				String sqlCommand = "SELECT * FROM Countries";
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(sqlCommand);
				
				countries = readCountries(rs);
				
				rs.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			this.closeConnection();
		}
		
		return countries;
	}
	
	@Override
	public boolean insert(ArrayList<Country> countries) {
		boolean flag = true;
		
		if (countries.size() == 0) return false;
		
		if (this.connect()) {
			
			try {
				String sqlCommand = "INSERT INTO Countries(Id, Continent, Country, "
						+ "Capital, Area, Population, TypeGovernment, Minerals)"
						+ " VALUES ";
				Statement statement = connection.createStatement();
				
				int len = countries.size();
				
				for (int i = 0; i < len; i++) {
					Country item = countries.get(i);
					
					String sqlValue = String.format("(%d, '%s', '%s', '%s', %d, %d, '%s', '%s')",
							item.getId(), item.getContinent(), item.getName(),
							item.getCapital(), item.getArea(),
							item.getPopulation(), item.getTypeGovernment(),
							item.getMinerals());
					
					String separator = (i < len - 1) ? ", " : "";
					sqlCommand += sqlValue + separator;
				}
				
				if (statement.executeUpdate(sqlCommand) == 0) flag = false;
				
				statement.close();
			} catch (SQLException e) {
				flag = false;
				e.printStackTrace();
			}
			
			this.closeConnection();
		}
		return flag;
	}
	
	@Override
	public boolean update(Country country) {
		boolean flag = true;
				
		if (this.connect()) {
			
			try {
				String sqlCommand = "UPDATE Countries SET ";
				Statement statement = connection.createStatement();

				String sqlValue = String.format("Continent = '%s', Country "
						+ "= '%s', Capital = '%s', Area = %d, Population = %d,"
						+ " TypeGovernment = '%s', Minerals = '%s'",
						country.getContinent(), country.getName(),
						country.getCapital(), country.getArea(),
						country.getPopulation(), country.getTypeGovernment(),
						country.getMinerals());
				sqlCommand += sqlValue + " WHERE Id = " + country.getId();
				
				if (statement.executeUpdate(sqlCommand) == 0) flag = false;
				
				statement.close();
			} catch (SQLException e) {
				flag = false;
				e.printStackTrace();
			}
			
			this.closeConnection();
		}
		return flag;
	}
	
	@Override
	public boolean delete(int id) {
		boolean flag = true;
		
		if (this.connect()) {
			
			try {
				Statement statement = connection.createStatement();
				String sqlCommand = "DELETE FROM Countries WHERE Id = " + id;
				
				if (statement.executeUpdate(sqlCommand) == 0) flag = false;
				
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			this.closeConnection();
		}
		
		return flag;
	}
	
	@Override
	public boolean truncate() {
		boolean flag = true;
		
		if (this.connect()) {
			
			try {
				Statement statement = connection.createStatement();
				String sqlCommand = "truncate table Countries";
				statement.execute(sqlCommand);
				statement.close();
			} catch (SQLException e) {
				flag = false;
				e.printStackTrace();
			}
			
			this.closeConnection();
		} else
			flag = false;
		
		return flag;
	}
	
	@Override
	public Country getCountry(int id) {
		ArrayList<Country> countries = new ArrayList<Country>();
		
		if (this.connect()) {
			
			try {
				Statement statement = connection.createStatement();
				String sqlCommand = "SELECT * FROM Countries WHERE Id = " + id;
				ResultSet rs = statement.executeQuery(sqlCommand);
				
				countries = readCountries(rs);
				
				rs.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			this.closeConnection();
		}
		
		return countries.get(0);
	}
	
	@Override
	public int getLastId() {
		int id = 1;
		
		String sqlCommand = "SELECT * FROM Countries ORDER BY Id DESC LIMIT 1";
		ArrayList<Country> items = query(sqlCommand);
		id = items.get(0).getId();
		
		return id;
	}
	
	@Override
	public String toString() {
		return String.format("БД MySQL: (%s?username=%s)", url, username);
	}
	
	@Override
	public Filter createFilter() {
		return new FilterMySql();
	}
}
