package ru.bstu.it32.anistratov.lab5;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
 
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class DbXml implements DataBase {
	private static ArrayList<Country> countries = new ArrayList<>();
	
	private static volatile DbXml instance;
	
	private DbXml() {
	}
	
	public static DbXml getInstance() {
		DbXml result = instance;
		
		if (result != null) {
			return result;
		}
		
		synchronized (DbXml.class) {
            if (instance == null) {
                instance = new DbXml();
            }
            
            return instance;	
		}
	}
	
	private static String filePath;

	public static String getFilePath() {
		return filePath;
	}

	public static void setFilePath(String path) {
		filePath = path;
	}

	/**
	 * Создает документ DOM на основе списка Countries. Может вернуть
	 * ошибку, которую нужно обрабатывать.
	 *  
	 * @param Countries список стран
	 * @return документ DOM
	 * @exception ParserConfigurationException
	 */
	private Document getDOM(ArrayList<Country> Countries) throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();
		
		Element root = doc.createElement("countries");
		doc.appendChild(root);
		
		for (Country country : Countries) {
			// создать узел, добавить текст и атрибуты
			Element child = doc.createElement("country");
			child.setTextContent(country.getName());

			String number = Integer.toString(country.getId());
			child.setAttribute("id", number);
			
			number = Integer.toString(country.getArea());
			child.setAttribute("area", number);
			
			number = Integer.toString(country.getPopulation());
			child.setAttribute("population", number);
			
			child.setAttribute("capital", country.getCapital());
			child.setAttribute("continent", country.getContinent());
			child.setAttribute("minerals", country.getMinerals());
			child.setAttribute("typeGovernment", country.getTypeGovernment());
			
			root.appendChild(child);
		}
		
		
		return doc;
	}
	
	/**
	 * Записывает список стран в xml файл.
	 * 
	 * @param countries массив объектов Country
	 * @return При успешной записи возвращает true, иначе - false
	 */
	private boolean XmlSave(ArrayList<Country> Countries) {
		boolean flag = true;
		
		try {
			Document doc = this.getDOM(Countries);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			
			DOMSource source = new DOMSource(doc);
			StreamResult file = new StreamResult(new File(filePath));
			transformer.transform(source, file);
			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		
		return flag;
	}
	
	@Override
	public Country getCountry(int id) {
		countries = this.all();
		
		for (Country country : countries) {
			if (country.getId() == id) {
				return country;
			}
		}
		
		return null;
	}
	
	@Override
	public int getLastId() {
		countries = this.all();
		int id = 1;
		int size = countries.size();
		
		if (size > 0) {
			Country lastCountry = countries.get(size - 1); 
			id = lastCountry.getId() + 1;
		}
		return id;
	}
	
	@Override
	public ArrayList<Country> all() {
		countries.clear();
		
		try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            SAXHandler handler = new SAXHandler();
            
            parser.parse(new File(filePath), handler);
        } catch (Exception e) {
        	e.printStackTrace();
		}
		
		return countries;
	}

	@Override
	public boolean insert(ArrayList<Country> Countries) {
		countries = all();
		countries.addAll(Countries);
		return XmlSave(countries);
	}
	
	@Override
	public boolean update(Country country) {
		boolean flag = false;
		
		// поиск записи с совпадающим id и его замена
		countries = this.all();
		int size = countries.size();
		
		if (size > 0) {
			for (int i = 0; i < countries.size(); ++i) {
				if (countries.get(i).getId() == country.getId()) {
					countries.set(i, country);
					flag = true;
					break;
				}
			}			
		}
		
		if (flag) {
			flag = XmlSave(countries);
		}
		
		return flag;
	}
	
	@Override
	public boolean delete(int id) {
		boolean flag = false;
		countries = this.all();
		int size = countries.size();
		
		if (size > 0) {
			for (int i = 0; i < countries.size(); ++i) {
				if (countries.get(i).getId() == id) {
					countries.remove(i);
					flag = true;
					break;
				}
			}			
		}
		
		if (flag) {
			flag = XmlSave(countries);
		}
		
		return flag;
	}

	@Override
	public boolean truncate() {
		countries.clear();
		
		boolean flag = XmlSave(countries);
		
		return flag;
	}
	
	@Override
	public String toString() {
		return String.format("БД XML: (%s)", filePath);
	}
	
	@Override
	public Filter createFilter() {
		return new FilterXml();
	}
	
	/**
	 * Обработчик для SAX. Создает объекты country и добавляет их в список countries
	 * @author DimJe
	 *
	 */
	private static class SAXHandler extends DefaultHandler {
		private String lastElement, name;
		private Country country;
		
	    @Override
	    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
	        lastElement = new String(qName);
	        country = new Country();
	        
	    	if (qName.equals("country")) {
	    		String atr = attributes.getValue("id");
	    		int number = 0; 
	    		
	    		try {
	    			number = Integer.parseInt(atr);
	    			
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    			number = 0;
				}
	    		country.setId(number);
	    		
	    		atr = attributes.getValue("area");
	    		try {
	    			number = Integer.parseInt(atr);
	    			
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    			number = 0;
				}
	    		country.setArea(number);
	    		
	    		atr = attributes.getValue("population");
	    		try {
	    			number = Integer.parseInt(atr);
	    			
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    			number = 0;
				}
	        	country.setPopulation(number);
	        	
	        	atr = attributes.getValue("capital");
	        	country.setCapital(atr);
	        	
	        	atr = attributes.getValue("continent");
	        	country.setContinent(atr);
	        	
	        	atr = attributes.getValue("minerals");
	        	country.setMinerals(atr);
	        	
	        	atr = attributes.getValue("typeGovernment");
	        	country.setTypeGovernment(atr);
	        }
	    }
	    
        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            name = new String(ch, start, length);
            
            name = name.replace("\n", "").trim();
            
            if (!name.isEmpty())
                if (lastElement.equals("country"))
                	country.setName(name);
        }
	    
        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (qName.equals("country")) {
            	countries.add(country);
            }
        }
	}

}
