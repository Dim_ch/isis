package ru.bstu.it32.anistratov.lab5;

public class Country {
	private int id;
	private String name;
	private String capital;
	private String continent;
	private int area;
	private int population;
	private String typeGovernment;
	private String minerals;
	
	public Country() {	
	}
	
	public Country(int id, String name, String capital, String continent, String typeGovernment, String minerals,
			int area, int population) {
		this.id = id;
		this.name = name;
		this.capital = capital;
		this.continent = continent;
		this.typeGovernment = typeGovernment;
		this.minerals = minerals;
		this.area = area;
		this.population = population;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null)
			this.name = "Не задано";
		else
			this.name = name;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		if (capital == null)
			this.capital = "Не задано";
		else
			this.capital = capital;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		if (continent == null)
			this.continent = "Не задано";
		else
			this.continent = continent;
	}

	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	public String getTypeGovernment() {
		return typeGovernment;
	}

	public void setTypeGovernment(String typeGovernment) {
		if (typeGovernment == null)
			this.typeGovernment = "Не задано";
		else
			this.typeGovernment = typeGovernment;
	}

	public String getMinerals() {
		return minerals;
	}

	public void setMinerals(String minerals) {
		if (minerals == null)
			this.minerals = "Не задано";
		else
			this.minerals = minerals;
	}
	
	@Override
	public String toString() {
		return String.format("ID: %d.\nКонтинент: %s.\nСтрана: %s.\nСтолица: %s."
				+ "\nПлощадь: %,d км2.\nНаселение: %,d чел.\nТип власти: %s.\n"
				+ "Полезные ископаемые: %s.", id, continent, name, capital,
				area, population, typeGovernment, minerals);
	}
}
