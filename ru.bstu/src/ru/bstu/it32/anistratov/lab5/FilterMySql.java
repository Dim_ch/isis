package ru.bstu.it32.anistratov.lab5;

import java.util.ArrayList;

public class FilterMySql implements Filter {
	
	private ArrayList<String> terms = new ArrayList<String>();
	
	private String getTermFilter() {
		String sqlQuery = "SELECT * FROM Countries WHERE ";

		int len = terms.size();
		for (int i = 0; i < len; i++) {
			sqlQuery += String.format("%s", terms.get(i));
			sqlQuery += (i < len - 1) ? " AND " : "";
		}
		
		return sqlQuery;
	}
	
	@Override
	public void addTermContinent(String str) {
		String term = String.format("(Continent REGEXP '%s')", str);
		terms.add(term);
	}

	@Override
	public void addTermCountry(String str) {
		String term = String.format("(Country REGEXP '%s')", str);
		terms.add(term);
	}

	@Override
	public void addTermCapital(String str) {
		String term = String.format("(Capital REGEXP '%s')", str);
		terms.add(term);
	}

	@Override
	public void addTermTypeGov(String str) {
		String term = String.format("(TypeGovernment REGEXP '%s')", str);
		terms.add(term);
	}

	@Override
	public void addTermMinerals(String str) {
		String[] minerals = str.split(",");
		
		String term = "";
		
		for (int i = 0; i < minerals.length; i++) {
			String mineral = minerals[i].trim();
			minerals[i] = mineral;
			term += String.format("%s", mineral);
			term += (i < minerals.length - 1) ? "|" : "";
		}
		
		term = String.format("(Minerals REGEXP '%s')", term);
		terms.add(term);
	}

	@Override
	public void addTermArea(int number, int operation) {
		String term = "";
		
		switch (operation) {
		case 0: // больше
			term = String.format("(Area > %d)", number);
			break;

		case 1: // меньше
			term = String.format("(Area < %d)", number);
			break;
			
		default: // равно
			term = String.format("(Area = %d)", number);
			break;
		}
		
		terms.add(term);
	}

	@Override
	public void addTermPopulation(int number, int operation) {
		String term = "";
		
		switch (operation) {
		case 0: // больше
			term = String.format("(Population > %d)", number);
			break;

		case 1: // меньше
			term = String.format("(Population < %d)", number);
			break;
			
		default: // равно
			term = String.format("(Population = %d)", number);
			break;
		}
		
		terms.add(term);
	}

	@Override
	public ArrayList<Country> filter(DataBase db) {
		ArrayList<Country> countries = new ArrayList<Country>();
		
		if (db instanceof DbMySql) {
			DbMySql dbMysql = (DbMySql)db;
			String sqlCommand = getTermFilter();
			countries = dbMysql.query(sqlCommand);
		}
		
		return countries;
	}

	private Country selectCountryByNumber(DataBase db, String sqlCommand) {
		ArrayList<Country> countries = new ArrayList<Country>();
		
		if (db instanceof DbMySql) {
			DbMySql dbMysql = (DbMySql)db;
			countries = dbMysql.query(sqlCommand);
		}
		
		return countries.get(0);
	}
	
	@Override
	public Country findCountryMaxArea(DataBase db) {
		String sqlCommand = "SELECT * FROM Countries ORDER BY Area DESC LIMIT 1";
		return selectCountryByNumber(db, sqlCommand);
	}

	@Override
	public Country findCountryMinArea(DataBase db) {
		String sqlCommand = "SELECT * FROM Countries ORDER BY Area ASC LIMIT 1";
		return selectCountryByNumber(db, sqlCommand);
	}

	@Override
	public Country findCountryMaxPopulation(DataBase db) {
		String sqlCommand = "SELECT * FROM Countries ORDER BY Population DESC LIMIT 1";
		return selectCountryByNumber(db, sqlCommand);
	}

	@Override
	public Country findCountryMinPopulation(DataBase db) {
		String sqlCommand = "SELECT * FROM Countries ORDER BY Population ASC LIMIT 1";
		return selectCountryByNumber(db, sqlCommand);
	}

}
