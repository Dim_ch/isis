package ru.bstu.it32.anistratov.lab4;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class App {
	public static void main(String[] args) {
		Scanner io = new Scanner(System.in);
		
		String filePath = "";
		File fileHtml;
		File fileOut;
		
		while (true) {
			System.out.print("Введите путь к файлу: ");
			filePath = io.nextLine();
			fileHtml = new File(filePath);
			
			if (fileHtml.exists()) {
				if (fileHtml.isFile()) {
					if (fileHtml.canRead()) break;
					else System.out.println("Файл " + filePath + "не может быть прочитан");
				}
				else System.out.println(filePath + " является каталогом");
			}
			else System.out.println("Файл или каталог не существует");
		}
		
		while (true) {
			System.out.print("Введите имя выходного файла: ");
			filePath = io.nextLine();
			fileOut = new File(filePath);
			
			if (!fileOut.exists()) {
				try {
					if (fileOut.createNewFile()) break;
					else System.out.println("Не удалось создать файл");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else System.out.println("Файл или каталог уже существует");
		}
		
		if (ParserEmail.readFile(fileHtml)) {
			try (FileWriter writer = new FileWriter(fileOut, false))
			{
				writer.write(ParserEmail.parse());
				System.out.println(ParserEmail.getParseEmails());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else System.out.println("Не удалось прочитать файл");
		
		io.close();
	}
}
