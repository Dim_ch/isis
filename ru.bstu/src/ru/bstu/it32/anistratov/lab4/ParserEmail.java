package ru.bstu.it32.anistratov.lab4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserEmail {
	private static String fileHtml;
	private static String parseEmails;
	
	/**
	 * @return the fileHtml
	 */
	public static String getFileHtml() {
		return fileHtml;
	}

	/**
	 * @param fileHtml the fileHtml to set
	 */
	public static void setFileHtml(String fileHtml) {
		ParserEmail.fileHtml = fileHtml;
	}
	
	public static String getParseEmails() {
		return parseEmails;
	}
	
	/***
	 * Читает файл и сохраняет данные в строку
	 * @param fileIn входной файл
	 * @return возвращает true, если удалось прочитать файл, иначе false
	 */
	public static boolean readFile(File fileIn) {
		boolean result = false;
		try (Scanner file = new Scanner(fileIn)) {
			fileHtml = file.useDelimiter("\\A").next();
			result = true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	/***
	 * 
	 * @return
	 */
	public static String parse() {
		String emails = "";
		String regexp = "([a-z\\d\\+]([a-z\\d]*[\\-_\\.]?)*[a-z\\d]+)@([a-zа-я\\d](([a-zа-я\\d]*[\\-_\\.]?)*\\.[a-zа-я\\d]+))";
		//String regexp = "([a-z\\d]([a-z\\d]*[\\-_\\.]?)*[a-z\\d]+)@([a-zа-я\\d](([a-zа-я\\d]*[\\-_\\.]?)*\\.[a-zа-я\\d]+))";
		Pattern pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(fileHtml);
		
		while (matcher.find()) {
			emails += matcher.group() + "\n";
		}

		parseEmails = emails;
		return emails;
	}

}
