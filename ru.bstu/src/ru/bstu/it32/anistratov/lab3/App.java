package ru.bstu.it32.anistratov.lab3;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {
	public static void main(String[] args) {
		Scanner io = new Scanner(System.in);
		
		int count = getCountHuman(io);
		boolean flag = false;
		Human[] humans = new Human[count];
		
		for (int i = 0; i < count; ++i) {
			if (flag) break;
			
			System.out.print("1 - школьник, 2 - студент, 3 - преподаватель,"
					+ " 4 - директор, 0 - выйти из цикла\n");
			
			switch(getNumber(io, 0, "Выберите тип персоны: ")) {
			case 0: 
				flag = true;
				break;
			case 1: 
				SchoolBoy boy = new SchoolBoy();
				boy.init(io);
				humans[i] = boy;
				break;
			case 2: 
				Student student = new Student();
				student.init(io);
				humans[i] = student;
				break;
			case 3: 
				Teacher teacher = new Teacher();
				teacher.init(io);
				humans[i] = teacher ;
				break;
			case 4: 
				Director director = new Director();
				director.init(io);
				humans[i] = director;
				break;
			default:
				i--;
			}
		}

		Human smallHuman = humans[0];
		
		for (int i = 0; i < humans.length - 1; ++i) {
			if (smallHuman != null && humans[i + 1] != null) {
				if (smallHuman.getAge() > humans[i + 1].getAge())
					smallHuman = humans[i + 1];				
			}
		}
		
		if (smallHuman != null)
			System.out.println(smallHuman);
		
		System.out.println("Конец программы.");
		io.close();
	}
	
	/**
	 * Считывает число с консоли, которое больше либо равно min
	 * @param scanner
	 * @param min минимальное число
	 * @param msg сообщение для приглашения ввода
	 * @return
	 */
	private static int getNumber(Scanner scanner, int min, String msg) {
		int number = min;
		String str = "";
		while(true) {
			System.out.print(msg);
			str = scanner.nextLine();
			try {
				number = Integer.parseInt(str);
				if (number >= min) break;
				else System.out.printf("Значение должно быть больше либо "
						+ "равно%s\n", min);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.printf("Не удалось преобразовать %s в число\n", str);
			}			
		}
		return number;
	}
	
	private static int getCountHuman(Scanner scan) {
		int count = 0;
		String str = "";
		Pattern pattern = Pattern.compile("\\d+");
		while (true) {
			System.out.print("Введите количество человек: ");
			str = scan.nextLine();
			Matcher matcher = pattern.matcher(str);
			
		    if (matcher.find()) {
		        count = Integer.parseInt(str.substring(matcher.start(), matcher.end()));
		        if (count > 0) break;
		    }
		}
		
		return count;
	}
}
