package ru.bstu.it32.anistratov.lab3;

import java.util.Calendar;
import java.util.Scanner;

public class Teacher extends Employee {
	private String post;

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}
	
	@Override
	public void init(Scanner scanner) {
		super.init(scanner);
		System.out.print("Введите академическую должность: ");
		post = scanner.nextLine();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String date = String.format("%s.%s.%s",
				dateBirth.get(Calendar.DAY_OF_MONTH),
				dateBirth.get(Calendar.MONTH) + 1,
				dateBirth.get(Calendar.YEAR));
		
		String str = String.format("ФИО: %s %s %s. Дата рождения: %s. "
				+ "Возраст: %d. Место работы: %s. Опыт работы: %d. Номер сертификата: "
				+ "%d. Должность: %s\n", lastName, firstName, patronymic, date,
				getAge(), getPlaceWork(), getExperience(), certificateNumber,
				post);
		return str;
	}
	
}
