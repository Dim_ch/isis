package ru.bstu.it32.anistratov.lab3;

import java.util.Calendar;
import java.util.Scanner;

public class Director extends Employee {
	private int salary;

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	@Override
	public void init(Scanner scanner) {
		super.init(scanner);
		salary = getNumber(scanner, 0, "Зарплата: ");
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String date = String.format("%s.%s.%s",
				dateBirth.get(Calendar.DAY_OF_MONTH),
				dateBirth.get(Calendar.MONTH) + 1,
				dateBirth.get(Calendar.YEAR));
		
		String str = String.format("ФИО: %s %s %s. Дата рождения: %s. "
				+ "Возраст: %d. Место работы: %s. Опыт работы: %d. Номер сертификата: "
				+ "%d. Зарплата: %d\n", lastName, firstName, patronymic, date,
				getAge(), getPlaceWork(), getExperience(), certificateNumber,
				salary);
		return str;
	}
}
