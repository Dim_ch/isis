package ru.bstu.it32.anistratov.lab3;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public abstract class Human {
	protected Calendar dateBirth;
	protected String firstName;
	protected String lastName;
	protected String patronymic;
	
	public Human() {
		dateBirth = new GregorianCalendar(2000, 0 , 1);
	}
	
	public String getFirstName() { return firstName; }
	
	public void setFirstName(String FirstName) { firstName = FirstName; }
	
	public String getLastName() { return lastName; }
	
	public void setLastName(String LastName) { lastName = LastName; }
	
	public String getPatronymic() { return patronymic; }
	
	public void setPatronymic(String Patronymic) { patronymic = Patronymic; }
	
	public Calendar getDateBirth() { return dateBirth; }
	
	public void setDateBirth(Calendar date) { dateBirth = date; }

	/**
	 * Считывает число с консоли, которое больше либо равно min
	 * @param scanner
	 * @param min минимальное число
	 * @param msg сообщение для приглашения ввода
	 * @return
	 */
	protected int getNumber(Scanner scanner, int min, String msg) {
		int number = min;
		String str = "";
		while(true) {
			System.out.print(msg);
			str = scanner.nextLine();
			try {
				number = Integer.parseInt(str);
				if (number >= min) break;
				else System.out.printf("Значение должно быть больше либо "
										+ "равно%s\n", min);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.printf("Не удалось преобразовать %s в число\n", str);
			}			
		}
		return number;
	}
	
	/**
	 * Считывает параметры с консоли
	 * @param scanner
	 */
	public void init(Scanner scanner) {
		System.out.print("Введите имя: ");
		firstName = scanner.nextLine();
		System.out.print("Введите фамилию: ");
		lastName = scanner.nextLine();
		System.out.print("Введите отчество: ");
		patronymic = scanner.nextLine();
		
		Calendar date = new GregorianCalendar();
		
		date.set(Calendar.YEAR,
				getNumber(scanner, 1000, "Введите год рождения: "));
		date.set(Calendar.MONTH,
				getNumber(scanner, 1, "Введите номер месяца рождения: ") - 1);
		date.set(Calendar.DAY_OF_MONTH,
				getNumber(scanner, 1, "Введите день месяца: "));
		
		dateBirth = date;
	}
	
	/**
	 * Возвращает количество полных лет
	 */
	public int getAge() {
		// TODO Auto-generated method stub
		Calendar nowDate = new GregorianCalendar();
		
		int age = nowDate.get(Calendar.YEAR) - dateBirth.get(Calendar.YEAR);
		
		if (dateBirth.get(Calendar.MONTH) > nowDate.get(Calendar.MONTH) || 
			(nowDate.get(Calendar.MONTH) == dateBirth.get(Calendar.MONTH) &&
				dateBirth.get(Calendar.DAY_OF_MONTH) <
				nowDate.get(Calendar.DAY_OF_MONTH))
			) {
			age--;			
		}
		
		return age;
	}
}
