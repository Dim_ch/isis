package ru.bstu.it32.anistratov.lab3;

import java.util.Scanner;

public abstract class Employee extends Human {
	protected String placeWork;
	protected int experience;
	protected int certificateNumber;

	public String getPlaceWork() {
		return placeWork;
	}

	public void setPlaceWork(String placeWork) {
		this.placeWork = placeWork;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public int getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(int certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	
	@Override
	public void init(Scanner scanner) {
		super.init(scanner);
		
		System.out.print("Введите место работы: ");
		placeWork = scanner.nextLine();
		experience = getNumber(scanner, 0, "Введите опыт работы: ");
		certificateNumber = getNumber(scanner, 0, "Введите номер сертификата: ");
	}
}
