package ru.bstu.it32.anistratov.lab3;

import java.util.Calendar;
import java.util.Scanner;

public class SchoolBoy extends Human {
	private String school;
	private int classroom;
	
	public String getSchool() { return school; }

	public void setSchool(String school) { this.school = school; }

	public int getClassroom() { return classroom; }

	public void setClassroom(int classroom) { this.classroom = classroom; }
	
	@Override
	public void init(Scanner scanner) {
		super.init(scanner);
		System.out.print("Введите название школы: ");
		school = scanner.nextLine();
		classroom = getNumber(scanner, 1, "Введите номер класса: ");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String date = String.format("%s.%s.%s",
				dateBirth.get(Calendar.DAY_OF_MONTH),
				dateBirth.get(Calendar.MONTH) + 1,
				dateBirth.get(Calendar.YEAR));
		
		String str = String.format("ФИО: %s %s %s. Дата рождения: %s. "
				+ "Возраст: %d. Школа: %s. Класс: %d\n", lastName,
				firstName, patronymic, date, getAge(), school, classroom);
		return str;
	}
	
}
