package ru.bstu.it32.anistratov.lab3;

import java.util.Calendar;
import java.util.Scanner;

public class Student extends Human{
	
	private String higherSchool;
	private String groupName;
	private int groupNumber;
	private int creditCardNumber;
	
	/**
	 * @return the higherSchool
	 */
	public String getHigherSchool() {
		return higherSchool;
	}

	/**
	 * @param higherSchool the higherSchool to set
	 */
	public void setHigherSchool(String higherSchool) {
		this.higherSchool = higherSchool;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the groupNumber
	 */
	public int getGroupNumber() {
		return groupNumber;
	}

	/**
	 * @param groupNumber the groupNumber to set
	 */
	public void setGroupNumber(int groupNumber) {
		this.groupNumber = groupNumber;
	}

	/**
	 * @return the creditCardNumber
	 */
	public int getCreditCardNumber() {
		return creditCardNumber;
	}

	/**
	 * @param creditCardNumber the creditCardNumber to set
	 */
	public void setCreditCardNumber(int creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	
	public String getFirstName() { return firstName; }
	
	public void setFirstName(String FirstName) { firstName = FirstName; }
	
	public String getLastName() { return lastName; }
	
	public void setLastName(String LastName) { lastName = LastName; }
	
	public String getPatronymic() { return patronymic; }
	
	public void setPatronymic(String Patronymic) { patronymic = Patronymic; }
	
	public Calendar getDateBirth() { return dateBirth; }
	
	public void setDateBirth(Calendar date) { dateBirth = date; }

	@Override
	public void init(Scanner scanner) {
		super.init(scanner);
		System.out.print("Введите название ВУЗа: ");
		higherSchool = scanner.nextLine();
		
		System.out.print("Введите название группы: ");
		groupName = scanner.nextLine();
		
		groupNumber = getNumber(scanner, 0, "Введите номер группы: ");
		
		creditCardNumber = getNumber(scanner, 0,
							"Введите номер зачетной книжки: ");
		
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String date = String.format("%s.%s.%s",
				dateBirth.get(Calendar.DAY_OF_MONTH),
				dateBirth.get(Calendar.MONTH) + 1,
				dateBirth.get(Calendar.YEAR));
		
		String str = String.format("ФИО: %s %s %s. Дата рождения: %s. "
				+ "Возраст: %d. ВУЗ: %s. Группа: %s-%d. Номер зачетной "
				+ "книжки: %d\n", lastName, firstName, patronymic, date,
				getAge(), higherSchool, groupName, groupNumber,
				creditCardNumber);
		return str;
	}
	
}
