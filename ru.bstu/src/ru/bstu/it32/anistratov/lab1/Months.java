package ru.bstu.it32.anistratov.lab1;

import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *  Класс Months
 * 
 * <p>
 * Класс преобразует число в название месяца
 * </p>
 */
public class Months {
	static final Logger Logger = LogManager.getLogger(Months.class);
	/** i номер месяца */
	private int i;

	//  Конструкторы
	public Months() {
		i = 0;
		Logger.info("Был создан объект класса Months");
	}

	public Months(int I) {
		i = I;
		Logger.info("Был создан объект класса Months");
	}

	// Методы
	/**
	 * Сеттер для поля i
	 * 
	 * @param I целое число (номер месяца)
	 * @return возвращает true, если удалось установить значение, иначе false
	 */
	public boolean setI(int I) {
		if (I > 0) {
			i = I;
			Logger.info("Номер месяца был успешно получен");
			return true;
		}
		Logger.warn("Не удалось установить номер месяца. Получено значение: " + I);
		return false;
	}

	public int getI() {
		return i;
	}

	/**
	 * Получает число с консоли
	 * 
	 * @param io Scanner io
	 */
	public void getDay(Scanner io) {
		while (true) {
			System.out.print("Введите номер месяца: ");

			if (io.hasNextInt()) {
				if (setI(io.nextInt()))
					break;
			}

			System.out.print("Введите положительное число\n");
			io.next();
		}
	}

	/**
	 * Получает данные из файла
	 * 
	 * @param filePath путь к файлу
	 */
	public void getDay(String filePath) {
		Logger.info("Начато получение номера месяца из файла");
		try (FileReader reader = new FileReader(new File(filePath)); Scanner scan = new Scanner(reader)) {
			Logger.info("Файл был успешно октрыт");

			if (!scan.hasNextInt()) {
				throw new Exception("Файл содержит некорректные данные: " + scan.nextLine() + "\n");
			}

			setI(scan.nextInt());
		} catch (Exception ex) {
			String message = String.format("Ошибка при чтении файла %s.\n", filePath);
			Logger.error(message, ex);
		}
	}

	/**
	 * Метод возвращает название месяца по числу, которое храниться в <code>i</code>
	 * 
	 * @return строка с называнием месяца
	 */
	public String getMonth() {
		String month = "Нет такого месяца";

		switch (this.i) {
		case 1:
			month = "январь";
			break;

		case 2:
			month = "февраль";
			break;

		case 3:
			month = "март";
			break;

		case 4:
			month = "апрель";
			break;

		case 5:
			month = "май";
			break;

		case 6:
			month = "июнь";
			break;

		case 7:
			month = "июль";
			break;

		case 8:
			month = "август";
			break;

		case 9:
			month = "сентябрь";
			break;

		case 10:
			month = "октябрь";
			break;

		case 11:
			month = "ноябрь";
			break;

		case 12:
			month = "декабрь";
			break;
		}
		return month;
	}
}
