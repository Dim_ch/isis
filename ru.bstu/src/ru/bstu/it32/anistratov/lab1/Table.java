package ru.bstu.it32.anistratov.lab1;

import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Класс Table
 * 
 * <p>
 * Выводит таблицу умножения и сложения натуральных чисел в десятичной системе
 * счисления на консоль
 * </p>
 */
public class Table {
	static final Logger Logger = LogManager.getLogger(Table.class);
	/** n количество столбцов и строк */
	private int n;

	// Констркуторы
	public Table() {
		n = 2;
		Logger.info("Создан объект класса Table");
	}

	public Table(int N) {
		n = N;
		Logger.info("Создан объект класса Table");
	}

	// Методы
	/**
	 * Сеттер для поля n
	 * 
	 * @param N целое число
	 * @return возвращает true, если удалось установить значение, иначе false
	 */
	public boolean setN(int N) {
		if (N > 0) {
			n = N;
			return true;
		}

		Logger.warn("Не удалось установить размер таблицы." + " Получено значение: " + N);
		return false;
	}

	/**
	 * @return the n
	 */
	public int getN() {
		return n;
	}

	/**
	 * Получает N из консоли
	 * 
	 * @param io Scanner io
	 */
	public void getData(Scanner io) {
		while (true) {
			System.out.print("Введите N: ");

			if (io.hasNextInt()) {
				if (setN(io.nextInt()))
					break;
			}

			System.out.print("N должно быть положительным числом\n");
			io.next();
		}
		Logger.info("Получены данные о размерах таблицы");
	}

	/**
	 * Считывает данные из файла
	 * 
	 * @param filePath Путь к файлу
	 */
	public void getData(String filePath) {
		Logger.info("Начато получение размера таблицы из файла");
		try (FileReader reader = new FileReader(new File(filePath)); Scanner scan = new Scanner(reader)) {
			Logger.info("Файл был открыт");

			if (!scan.hasNextInt()) {
				throw new Exception("Файл содержит некорректные данные: " + scan.nextLine() + "\n");
			}

			setN(scan.nextInt());
		} catch (Exception ex) {
			String message = String.format("Ошибка при чтении файла %s.\n", filePath);
			Logger.error(message, ex);
		}
	}

	/**
	 * Выводит таблицу сложения на консоль, используя цикл for
	 * 
	 * @return возвращает строку с таблицей сложения
	 */
	public String printAddition() {
		String tableStr = "Таблица сложения (for):\n";

		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= n; ++j) {
				tableStr += String.format("%d+%d=%d ", j, i, j + i);
			}

			tableStr += "\n";
		}

		tableStr += "\n";
		return tableStr;
	}

	/**
	 * Выводит таблицу умножения на консоль, используя цикл while
	 * 
	 * @return возвращает строку с таблицей умножения
	 */
	public String printMultiplicationWhile() {
		String tableStr = "Таблица умножения (while):\n";

		int i = 1, j;
		while (i < (n + 1)) {
			j = 1;

			while (j < (n + 1)) {
				tableStr += String.format("%d*%d=%d ", j, i, j * i);
				++j;
			}

			tableStr += "\n";
			++i;
		}

		tableStr += "\n";
		return tableStr;
	}
}
