package ru.bstu.it32.anistratov.lab1;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Anistratov Dmitry
 *         <p>
 *         Лабораторная работа №1
 *         </p>
 */
public class App {
	static final Logger Logger = LogManager.getLogger(App.class);

	public static void main(String[] args) {
		Logger.info("Начало работы консольного приложения");
		Scanner io = new Scanner(System.in);

		System.out.println("Считывать данные из файла - 1\n" + "Считывать данные с консоли - 2\n");

		int choice = 1;
		while (true) {
			System.out.print("Выберите способ ввода данных: ");
			if (io.hasNextInt()) {
				choice = io.nextInt();
				break;
			}
			io.next();
		}

		if (choice == 1) {
			final String dir = System.getProperty("user.dir");

			System.out.println("Условные выражения\n");
			Brick task1 = new Brick();
			task1.getData(dir + "\\task1.txt");

			try {
				System.out.printf("Кирпич %sпроходит через отверстие\n\n", task1.checkCondition() ? "" : "не ");
			} catch (Exception ex) {
				Logger.warn(ex.getMessage());
			}

			System.out.println("Оператор выбора\n");
			Months month = new Months();
			month.getDay(dir + "\\task2.txt");
			System.out.printf("%d - %s\n\n", month.getI(), month.getMonth());

			System.out.println("Циклы\n");
			Table table = new Table();
			table.getData(dir + "\\task3.txt");
			System.out.println(table.printAddition());
			System.out.println(table.printMultiplicationWhile());

			System.out.println("Массивы\n");
			Array array = new Array();
			array.getData(dir + "\\task4.txt");
			System.out.println("Количество отрицательных чисел: " + array.getCountNegativeNumber());
			System.out.println("Количество нулевых чисел: " + array.getCountZeroNumber());
			System.out.println("Количество положительных чисел: " + array.getCountPositiveNumber());
		} else {
			System.out.println("Условные выражения\n");
			Brick task1 = new Brick();
			task1.getData(io);

			try {
				System.out.printf("Кирпич %sпроходит через отверстие\n\n", task1.checkCondition() ? "" : "не ");
			} catch (Exception ex) {
				Logger.warn(ex.getMessage());
			}

			System.out.println("Оператор выбора\n");
			Months month = new Months();
			month.getDay(io);
			System.out.printf("%d - %s\n\n", month.getI(), month.getMonth());

			System.out.println("Циклы\n");
			Table table = new Table();
			table.getData(io);
			System.out.println(table.printAddition());
			System.out.println(table.printMultiplicationWhile());

			System.out.println("Массивы\n");
			Array array = new Array();
			array.getData(io);
			System.out.println("Количество отрицательных чисел: " + array.getCountNegativeNumber());
			System.out.println("Количество нулевых чисел: " + array.getCountZeroNumber());
			System.out.println("Количество положительных чисел: " + array.getCountPositiveNumber());
		}

		io.close();
		Logger.info("Конец работы консольного приложения");
	}
}
