package ru.bstu.it32.anistratov.lab1;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Класс Array
 * 
 * <p>
 * Подсчитывает количество отрицательных, положительных и нулевых чисел в
 * массиве действительных чисел.
 * </p>
 */
public class Array {
	static final Logger Logger = LogManager.getLogger(Array.class);

	private Double[] arr;

	private int countNegativeNumber;
	private int countZeroNumber;
	private int countPositiveNumber;

	// Конструкторы
	public Array() {
		Logger.info("Создан объект класса Array");
	}

	public Array(Double[] array) {
		arr = new Double[array.length];
		for (int i = 0; i < array.length; ++i) {
			arr[i] = array[i];
		}
		Logger.info("Создан объект класса Array");
	}

	public Array(String array) {
		setArr(array);
	}

	public int getCountNegativeNumber() {
		return countNegativeNumber;
	}

	public int getCountZeroNumber() {
		return countZeroNumber;
	}

	public int getCountPositiveNumber() {
		return countPositiveNumber;
	}

	// Методы
	public void setArr(double[] array) {
		arr = new Double[array.length];
		for (int i = 0; i < array.length; ++i) {
			arr[i] = array[i];
		}

		Logger.info("Получен массив из типа double[]");
		ComputeParametrs();
	}

	/**
	 * Получает массив из строки
	 * 
	 * @param array строка с массивом
	 */
	public void setArr(String array) {
		Scanner scan = new Scanner(array);
		ArrayList<Double> arrList = new ArrayList<Double>();

		while (scan.hasNextDouble()) {
			arrList.add(scan.nextDouble());
		}

		arr = arrList.toArray(new Double[0]);

		Logger.info("Создан массив из строки");
		ComputeParametrs();
		scan.close();
	}

	/**
	 * Считывает данные с консоли
	 * 
	 * @param io Scanner io
	 */
	public void getData(Scanner io) {
		int N;

		while (true) {
			System.out.printf("Введите количество элементов массива: ");

			if (io.hasNextInt()) {
				N = io.nextInt();
				if (N > 0)
					break;
			}

			System.out.printf("Длина массива должна быть числом больше 0.\n");
			io.next();
		}

		arr = new Double[N];
		for (int i = 0; i < N; ++i) {

			while (true) {
				System.out.printf("Введите {%d-й} элемент: ", i + 1);

				if (io.hasNextDouble()) {
					arr[i] = io.nextDouble();
					break;
				}

				System.out.printf("Введите действительное число\n");
				io.next();
			}
		}
		ComputeParametrs();
		Logger.info("Получен массив");
	}

	/**
	 * Считывает данные из файла
	 * 
	 * @param filePath путь к файлу
	 */
	public void getData(String filePath) {
		Logger.info("Начато получение массива из файла");
		try (FileReader reader = new FileReader(new File(filePath)); Scanner scan = new Scanner(reader)) {
			Logger.info("Файл открыт");

			if (!scan.hasNextInt()) {
				throw new Exception("Файл содержит некорректные данные: " + scan.nextLine() + "\n");
			}

			int len = scan.nextInt();
			double[] array = new double[len];
			int i = 0;

			while (scan.hasNextDouble()) {
				array[i] = scan.nextDouble();
				++i;
			}

			Logger.info("Чтение данных завершилось");
			setArr(array);
		} catch (Exception ex) {
			Logger.error("Произошла ошибка при работе с файлом", ex);
		}
	}

	/**
	 * Подсчитывает количество положительных, отрицательных и нулевых чисел в
	 * массиве
	 */
	private void ComputeParametrs() {
		Logger.info("Начало подсчета количества положительных, отрицательных и нулевых чисел.");

		countPositiveNumber = 0;
		countNegativeNumber = 0;
		countZeroNumber = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > 0) {
				countPositiveNumber++;
			} else if (arr[i] < 0) {
				countNegativeNumber++;
			} else {
				countZeroNumber++;
			}
		}

		Logger.info("Конец подсчета.");
	}

	@Override
	public String toString() {
		String array = "";
		for (int i = 0; i < arr.length - 1; ++i)
			array += arr[i].toString() + " ";
		array += arr[arr.length - 1].toString();
		return array;
	}

}
