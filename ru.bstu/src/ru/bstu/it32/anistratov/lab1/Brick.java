package ru.bstu.it32.anistratov.lab1;

import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Класс Brick
 * 
 * <p>
 * Класс предназначен для решения слеюущей задачи: Заданы размеры А, В
 * прямоугольного отверстия и размеры х,у, z кирпича. Определить, пройдет ли
 * кирпич через отверстие.
 * </p>
 */
public class Brick {
	static final Logger Logger = LogManager.getLogger(Brick.class);
	/** x ширина кирпича */
	private int x;

	/** y длина кирпича */
	private int y;

	/** z высота кирпича */
	private int z;

	/** a ширина отверстия a */
	private int a;

	/** b ширина отверстия b */
	private int b;

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @return the z
	 */
	public int getZ() {
		return z;
	}

	/**
	 * @return the a
	 */
	public int getA() {
		return a;
	}

	/**
	 * @return the b
	 */
	public int getB() {
		return b;
	}

	// Конструкторы
	public Brick() {
		Logger.info("Создан объект класса Brick");
	}

	public Brick(int A, int B, int X, int Y, int Z) {
		x = X;
		y = Y;
		z = Z;
		a = A;
		b = B;
		Logger.info("Создан объект класса Brick");
	}

	// Методы
	public void setX(int X) {
		if (X > 0)
			x = X;
		else
			Logger.warn("Значение стороны кирпича X должно " + "быть положительным числом. Получено значение: " + X);
	}

	public void setY(int Y) {
		if (Y > 0)
			y = Y;
		else
			Logger.warn("Значение стороны кирпича Y должно " + "быть положительным числом. Получено значение: " + Y);
	}

	public void setZ(int Z) {
		if (Z > 0)
			z = Z;
		else
			Logger.warn("Значение стороны кирпича Z должно " + "быть положительным числом. Получено значение: " + Z);
	}

	public void setA(int A) {
		if (A > 0)
			a = A;
		else
			Logger.warn("Значение размера отверстия A должно " + "быть положительным числом. Получено значение: " + A);
	}

	public void setB(int B) {
		if (B > 0)
			b = B;
		else
			Logger.warn("Значение размера отверстия B должно " + "быть положительным числом. Получено значение: " + B);
	}

	/**
	 * Получает из консоли число
	 * 
	 * @param io        Scanner io
	 * @param fieldName Название поля
	 * @return Возвращает число <code>int</code>
	 */
	public int getNumber(Scanner io, String fieldName) {
		int number = 0;

		while (true) {
			System.out.printf("%s: ", fieldName);
			if (io.hasNextInt()) {
				number = io.nextInt();
				if (number > 0)
					break;
			}
			System.out.print("Введите положительное число\n");
			io.next();
		}
		return number;
	}

	/**
	 * Получает размеры кирпича и отверстия с консоли
	 * 
	 * @param io (Scanner io)
	 */
	public void getData(Scanner io) {
		Logger.info("Начато получение данных о размерах кирпича и отверстия");
		System.out.print("Введите размеры сторон отверстия\n");
		setA(getNumber(io, "A"));
		setB(getNumber(io, "B"));

		System.out.print("Введите размеры сторон кирпича\n");
		setX(getNumber(io, "x"));
		setY(getNumber(io, "y"));
		setZ(getNumber(io, "z"));
		Logger.info("Данные были получены");
	}

	/**
	 * Получает даные из файла
	 * 
	 * @param filePath путь к файлу с данными
	 */
	public void getData(String filePath) {
		Logger.info("Начато получение данных о размерах кирпича и отверстия");
		try (FileReader reader = new FileReader(new File(filePath)); Scanner scan = new Scanner(reader)) {
			Logger.info("Файл был открыт");
			if (!scan.hasNextInt()) {
				throw new Exception("Файл содержит некорректные данные: " + scan.nextLine() + "\n");
			}
			setA(scan.nextInt());

			if (!scan.hasNextInt()) {
				throw new Exception("Файл содержит некорректные данные: " + scan.nextLine() + "\n");
			}
			setB(scan.nextInt());

			if (!scan.hasNextInt()) {
				throw new Exception("Файл содержит некорректные данные: " + scan.nextLine() + "\n");
			}
			setX(scan.nextInt());

			if (!scan.hasNextInt()) {
				throw new Exception("Файл содержит некорректные данные: " + scan.nextLine() + "\n");
			}
			setY(scan.nextInt());

			if (!scan.hasNextInt()) {
				throw new Exception("Файл содержит некорректные данные: " + scan.nextLine() + "\n");
			}
			setZ(scan.nextInt());
			Logger.info("Данные из файла были считаны");

		} catch (Exception ex) {
			String message = String.format("Ошибка при чтении файла %s.\n%s", filePath, ex.getMessage());
			Logger.error(message, ex);
		}
	}

	/**
	 * Проверяет условие задачи
	 * 
	 * @return Если кирпич проходит через отверстие, возвращается true, иначе false.
	 * @throws Exception Генерируется Exception, если не все поля положительны
	 */
	public boolean checkCondition() throws Exception {
		if (x < 1 || y < 1 || z < 1 || a < 1 || b < 1)
			throw new Exception("Не все поля класса положительны");

		if (x < a) {
			if ((y < b) || (z < b))
				return true;
		}
		if (y < a) {
			if ((x < b) || (z < b))
				return true;
		}
		if (z < a) {
			if ((x < b) || (y < b))
				return true;
		}
		return false;
	}
}
