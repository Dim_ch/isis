package application;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ru.bstu.it32.anistratov.lab4.ParserEmail;

public class SampleController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField fieldFilePath;

    @FXML
    private Button btnOpen;

    @FXML
    private TextArea fieldInText;

    @FXML
    private TextArea fieldOutEmail;

    @FXML
    void btnOpenClick(ActionEvent event) {
		Node node = (Node) event.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
    	FileChooser fileChooser = getFileChooser();
		File file = fileChooser.showOpenDialog(stage);
		
		if (file != null) {
			fieldFilePath.setText(file.getAbsolutePath());
			if (ParserEmail.readFile(file)) {
				fieldInText.setText(ParserEmail.getFileHtml());
				fieldOutEmail.setText(ParserEmail.parse());
			}
			else {
				fieldInText.setText("Не удалось прочитать файл");
			}
		}
	    	
    }

	private FileChooser getFileChooser() {
		FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("*.html", "*.html");
		FileChooser fileChooser = new FileChooser();

		fileChooser.setTitle("Открыть");
		fileChooser.getExtensionFilters().add(filter);
		
		return fileChooser;
	}

    @FXML
    void initialize() {
        assert fieldFilePath != null : "fx:id=\"fieldFilePath\" was not injected: check your FXML file 'Sample.fxml'.";
        assert btnOpen != null : "fx:id=\"btnOpen\" was not injected: check your FXML file 'Sample.fxml'.";
        assert fieldInText != null : "fx:id=\"fieldInText\" was not injected: check your FXML file 'Sample.fxml'.";
        assert fieldOutEmail != null : "fx:id=\"fieldOutEmail\" was not injected: check your FXML file 'Sample.fxml'.";

    }
}
