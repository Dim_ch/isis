package application;

import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javafx.event.Event;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.Tab;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.control.TextArea;

import ru.bstu.it32.anistratov.lab1.*;

public class SampleController implements Initializable {
	static final Logger Logger = LogManager.getLogger(SampleController.class);
	final String USER_DIR = System.getProperty("user.dir");
	
	SpinnerValueFactory<Integer> svfX = new SpinnerValueFactory.IntegerSpinnerValueFactory(-10, 1000, 1);
	SpinnerValueFactory<Integer> svfY = new SpinnerValueFactory.IntegerSpinnerValueFactory(-10, 1000, 1);
	SpinnerValueFactory<Integer> svfZ = new SpinnerValueFactory.IntegerSpinnerValueFactory(-10, 1000, 1);
	SpinnerValueFactory<Integer> svfA = new SpinnerValueFactory.IntegerSpinnerValueFactory(-10, 1000, 1);
	SpinnerValueFactory<Integer> svfB = new SpinnerValueFactory.IntegerSpinnerValueFactory(-10, 1000, 1);
	SpinnerValueFactory<Integer> svfMonth = new SpinnerValueFactory.IntegerSpinnerValueFactory(-10, 13, 1);
	SpinnerValueFactory<Integer> svfTableN = new SpinnerValueFactory.IntegerSpinnerValueFactory(-10, 20, 1);
	
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;
    
    @FXML
    private Label task1Result;

    @FXML
    private Spinner<Integer> spinnerX;
    @FXML
    private Spinner<Integer> spinnerY;
    @FXML
    private Spinner<Integer> spinnerZ;
    @FXML
    private Spinner<Integer> spinnerA;
    @FXML
    private Spinner<Integer> spinnerB;    
    @FXML
    private Spinner<Integer> spinnerMonth;
    @FXML
    private Spinner<Integer> spinnerTableN;

	@FXML
	private Button btnCheck;
	@FXML
	private Button btnOkTable;
	@FXML
	private Button btnOkArray;	
    @FXML
    private Button btnMonthOk;
    @FXML
    private Button btnOpen1;
    @FXML
    private Button btnOpen2;
    @FXML
    private Button btnOpen3;
    @FXML
    private Button btnOpen4;
	
	@FXML
	private TextField textFieldMonth;
	@FXML
	private TextField textFieldArray;
    @FXML
    private TextField textFieldFile1;
    @FXML
    private TextField textFieldFile2;
    @FXML
    private TextField textFieldFile3;
    @FXML
    private TextField textFieldFile4;
    
    @FXML
    private TextArea textAreaTable;
    @FXML
    private TextArea textAreaLogger;
    
	@FXML
	private Label labelNegativeNum;
	@FXML
	private Label labelZeroNum;
	@FXML
	private Label labelPositiveNum;
	
    @FXML
    private Tab tabLog;
    
    @FXML
    void btnCheckClick(ActionEvent event) {
    	String message = "-----";
    	task1Result.setText(message);
    	try {
    		Brick brick = new Brick();
    		
    		brick.setA(spinnerA.getValue());
    		brick.setB(spinnerB.getValue());
    		brick.setX(spinnerX.getValue());
    		brick.setY(spinnerY.getValue());
    		brick.setZ(spinnerZ.getValue());

    		message = "Кирпич " + (brick.checkCondition() ? "" : "не ")
    						 + "проходит через отверстие";
    	} catch (Exception ex) {
			// TODO: handle exception
    		Logger.warn(ex.getMessage());
		}
    	task1Result.setText(message);
    }

    @FXML
    void btnMonthOkClick(ActionEvent event) {
    	try {
    		Months month = new Months();
    		month.setI(svfMonth.getValue());
    		
    		textFieldMonth.setText(month.getMonth());  		
    	} catch (Exception ex) {
    		Logger.warn(ex.getMessage());
    	}
    }
    
    @FXML
    void btnOkTableClick(ActionEvent event) {
    	try {
    		Table table = new Table();
    		table.setN(svfTableN.getValue());
    		textAreaTable.setText(table.printAddition() + 
    							  table.printMultiplicationWhile());
    	} catch (Exception ex) {
    		Logger.warn(ex.getMessage());
    	}
    }
    
    @FXML
    public void btnOkArrayClick(ActionEvent event) {
    	try {
    		String arrStr = textFieldArray.getText();
    		Array arr = new Array(arrStr);
    		
    		labelNegativeNum.setText("Количество отрицательных чисел: " +
    								 arr.getCountNegativeNumber());
    		labelPositiveNum.setText("Количество положительных чисел: " +
					 arr.getCountPositiveNumber());
    		labelZeroNum.setText("Количество нулевых чисел: " +
					 arr.getCountZeroNumber());
    	} catch(Exception ex) {
    		Logger.warn(ex.getMessage());
    	}
    }

	@FXML public void btnOpen1Click(ActionEvent event) {
		Node node = (Node) event.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		FileChooser fileChooser = getFileChooser();
		File file = fileChooser.showOpenDialog(stage);
		
		if (file != null) {
			Brick brick = new Brick();
			
			textFieldFile1.setText(file.getAbsolutePath());
			brick.getData(file.getAbsolutePath());

    		svfA.setValue(brick.getA());
    		svfB.setValue(brick.getB());
    		svfX.setValue(brick.getX());
    		svfY.setValue(brick.getY());
    		svfZ.setValue(brick.getZ());

    		String message = "";
    		try {
    			message = "Кирпич " + (brick.checkCondition() ? "" : "не ")
    					+ "проходит через отверстие";    			
    		} catch(Exception ex) {
    			message = ex.getMessage();
    			Logger.warn(ex.getMessage());
    		}

	    	task1Result.setText(message);
		}
	}
	
    @FXML
    void btnOpen2Click(ActionEvent event) {
		Node node = (Node) event.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		FileChooser fileChooser = getFileChooser();
		File file = fileChooser.showOpenDialog(stage);
		
		if (file != null) {
			Months month = new Months();
			
			textFieldFile2.setText(file.getAbsolutePath());
			month.getDay(file.getAbsolutePath());
			
			svfMonth.setValue(month.getI());
			textFieldMonth.setText(month.getMonth());
		}
    }
	
    @FXML
    void btnOpen3Click(ActionEvent event) {
		Node node = (Node) event.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		FileChooser fileChooser = getFileChooser();
		File file = fileChooser.showOpenDialog(stage);
		
		if (file != null) {
			Table table = new Table();
			
			textFieldFile3.setText(file.getAbsolutePath());
			table.getData(file.getAbsolutePath());;
			
			svfTableN.setValue(table.getN());
			textAreaTable.setText(table.printAddition() + 
					  			  table.printMultiplicationWhile());
		}
    }
    
    @FXML
    void btnOpen4Click(ActionEvent event) {
		Node node = (Node) event.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		FileChooser fileChooser = getFileChooser();
		File file = fileChooser.showOpenDialog(stage);
		
		if (file != null) {
			Array array = new Array();
			
			textFieldFile4.setText(file.getAbsolutePath());
			array.getData(file.getAbsolutePath());
			
			textFieldArray.setText(array.toString());
			
    		labelNegativeNum.setText("Количество отрицательных чисел: " +
    				array.getCountNegativeNumber());
			labelPositiveNum.setText("Количество положительных чисел: " +
					array.getCountPositiveNumber());
			labelZeroNum.setText("Количество нулевых чисел: " +
					array.getCountZeroNumber());
		}
    }
    
	private FileChooser getFileChooser() {
		FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("*.txt", "*.txt");
		FileChooser fileChooser = new FileChooser();

		fileChooser.setTitle("Открыть");
		fileChooser.getExtensionFilters().add(filter);
		
		return fileChooser;
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		tabLog.setOnSelectionChanged(event -> logTabSelection(event));
		
		spinnerX.setValueFactory(svfX);
		spinnerY.setValueFactory(svfY);		
		spinnerZ.setValueFactory(svfZ);		
		spinnerA.setValueFactory(svfA);		
		spinnerB.setValueFactory(svfB);		
		spinnerMonth.setValueFactory(svfMonth);		
		spinnerTableN.setValueFactory(svfTableN);		
		
		spinnerX.focusedProperty().addListener((obs, oldValue, newValue) -> {
			// Потеря фокуса
			if (!newValue) {
				if (svfX.getValue() == null) {
					svfX.setValue(1);
				}
			}
		});
		
		spinnerY.focusedProperty().addListener((obs, oldValue, newValue) -> {
			if (!newValue) {
				if (svfY.getValue() == null) {
					svfY.setValue(1);
				}
			}
		});
		
		spinnerZ.focusedProperty().addListener((obs, oldValue, newValue) -> {
			if (!newValue) {
				if (svfZ.getValue() == null) {
					svfZ.setValue(1);
				}
			}
		});
		
		spinnerA.focusedProperty().addListener((obs, oldValue, newValue) -> {
			if (!newValue) {
				if (svfA.getValue() == null) {
					svfA.setValue(1);
				}
			}
		});
		
		spinnerB.focusedProperty().addListener((obs, oldValue, newValue) -> {
			if (!newValue) {
				if (svfB.getValue() == null) {
					svfB.setValue(1);
				}
			}
		});
		
		spinnerMonth.focusedProperty().addListener((obs, oldValue, newValue) -> {
			if (!newValue) {
				if (svfMonth.getValue() == null) {
					svfMonth.setValue(1);
				}
			}
		});
		
		spinnerTableN.focusedProperty().addListener((obs, oldValue, newValue) -> {
			if (!newValue) {
				if (svfTableN.getValue() == null) {
					svfTableN.setValue(1);
				}
			}
		});

	}
	/**
	 * Выводит логи в текстовое поле.
	 */
	@FXML public void logTabSelection(Event event) {
		String path = USER_DIR + "\\logs\\debug.log";
		try (FileReader reader = new FileReader(new File(path));
				 Scanner in = new Scanner(reader)) {
			StringBuilder buf = new StringBuilder();
			
			// Чтение файла с логами
			while (true) {
				assert in != null;
				if (!in.hasNextLine()) break;
				buf.append(in.nextLine()).append("\n");
			}
			
			textAreaLogger.setText(buf.toString());
		} catch (Exception ex) {
			Logger.warn(ex.getMessage());
			textAreaLogger.setText("Не могу открыть файл с логами.");
		}
	}
}
